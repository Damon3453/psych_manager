<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Client, Session, ClientTherapy};

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $user = \App\Models\User::firstWhere('email', config('app.admin_email'));
        // todo: without global scopes (somehow)
        Client::factory()->count(60)->afterCreating(static function (Client $client) use ($user): void {
            $client->sessions()->saveManyQuietly(
                Session::factory()
                    ->count(30)
                    ->state(fn(array $attributes) => [
                        'user_id' => $user->id,
                        'client_id' => $client->id,
                    ])->create()
            );
        })->has(
            ClientTherapy::factory()
                ->state(fn(array $attributes, ?Client $client) => [
                    'client_id' => $client->id,
                ]),
            'therapy'
        )->create();
    }
}
