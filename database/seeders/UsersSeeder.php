<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::create([
            'name' => 'Admin',
            'email' => config('app.admin_email'),
            'password' => config('app.admin_password'),
            'phone' => config('app.admin_phone'),
        ]);
        User::create([
            'name' => 'Natalie',
            'email' => 'nataliemaslova@mail.ru',
            'password' => 'nataliemaslova@mail.ru',
            'phone' => '89182702625',
        ]);
        User::create([
            'name' => 'visitor',
            'email' => 'visitor@yandex.ru',
            'password' => 123456,
            'phone' => '1234',
        ]);
        User::create([
            'name' => 'Psych',
            'email' => 'psych@yandex.ru',
            'password' => 'psych@yandex.ru',
            'phone' => '88005553535',
        ]);
    }
}
