<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::dropIfExists('payment_notifications');
        Schema::create('payment_notifications', static function (Blueprint $table): void {
            $table->string('id')->index();
            $table->string('event');
            $table->foreignUuid('payment_method_id')
                ->nullable()
                ->references('payment_method_id')
                ->on('subscriptions')
                ->cascadeOnDelete();
            $table->string('status');
            $table->string('amount_paid')->comment('Заплатил пользователь');
            $table->string('amount_income')->nullable()->comment('Пришло по факту');
            $table->string('recipient_account_id');
            $table->string('recipient_gateway_id');
            $table->jsonb('payment_method');
            $table->timestamp('captured_at')->nullable();
            $table->boolean('paid')->default(true);
            $table->boolean('refundable')->default(true);
            $table->jsonb('authorization_details');
            $table->jsonb('cancellation_details')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('payment_notifications');
    }
};
