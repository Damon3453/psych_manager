<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('sessions', static function (Blueprint $table): void {
            $table->id();
            $table->foreignId('user_id')->index()->nullable()->constrained();
            $table->foreignId('client_id')->nullable()->constrained();

            $table->date('session_date')->index();
            $table->time('session_time_start');
            $table->time('session_time_end');
            $table->text('comment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('sessions');
    }
}
