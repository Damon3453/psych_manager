<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('subscriptions', static function (Blueprint $table): void {
            $table->id();
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->uuid('payment_method_id')
                ->index()
                ->nullable()
                ->comment('Id сохранённого способа оплаты. Первая оплата');
            $table->string('status');
            $table->tinyInteger('period')->comment('Продолжительность подписки в месяцах');
            $table->date('cancel_date');
            $table->string('amount')->comment('Сумма платежа');
            $table->jsonb('response')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('subscriptions');
    }
};
