<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

/**
 * @extends Factory<\App\Models\Client>
 */
class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @throws \Exception
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $users = User::query()->select(['id'])->get();

        return [
            'user_id' => $users->random()->id,
            'name' => $this->faker->firstName,
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->unique()->phoneNumber,
            'gender' => $this->faker->numberBetween(0, 1),
            'connection_type_id' => $this->faker->numberBetween(1, 6),
            'meeting_type' => $this->faker->numberBetween(0, 1),
        ];
    }
}
