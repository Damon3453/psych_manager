<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends Factory<\App\Models\Session>
 */
class SessionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @throws \Exception
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $today = Carbon::today();
        $date = $today->addDays(random_int(1, 14))->addHours(random_int(10, 18));

        return [
            'session_date' => $date->format('Y-m-d'),
            'session_time_start' => $date->startOfHour()->format('H:i'),
            'session_time_end' => $date->addHour()->format('H:i'),
            'comment' => $this->faker->realText(350),
        ];
    }
}
