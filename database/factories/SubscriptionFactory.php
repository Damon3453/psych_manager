<?php

declare(strict_types=1);

namespace Database\Factories;

use App\DataObjects\Payments\YooKassa\YooKassaSuccessResponseData;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class SubscriptionFactory extends Factory
{
    protected $model = Subscription::class;

    public function definition(): array
    {
        $response = YooKassaSuccessResponseData::from([
            'id' => $this->faker->uuid(),
            'status' => $this->faker->randomElement(['succeeded', 'pending', 'canceled']),
            'paid' => $this->faker->boolean(),
            'created_at' => Carbon::now()->format('Y-m-d'),
            'description' => $this->faker->sentence(),
        ]);

        return [
            'payment_method_id' => $response->id,
            'status' => $response->status,
            'period' => $this->faker->randomElement([0, 6, 12]),
            'cancel_date' => Carbon::now()->addWeeks(2),
            'amount' => $this->faker->randomElement(['1200', '10000', '50000']),
            'response' => $response,
            'created_at' => $response->createdAt,
            'updated_at' => $response->createdAt,

            'user_id' => User::factory(),
        ];
    }
}
