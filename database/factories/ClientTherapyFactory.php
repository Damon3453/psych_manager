<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<\App\Models\ClientTherapy>
 */
class ClientTherapyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'problem_severity' => $this->faker->numberBetween(1, 5),
            'plan' => $this->faker->realText(),
            'concept_vision' => $this->faker->realText(350),
            'request' => $this->faker->realText(100),
            'notes' => $this->faker->realText(300),
        ];
    }
}
