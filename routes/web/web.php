<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/', 'app');

// Route::get('/', static fn() => redirect('/panel'));
Route::view('/panel', 'app');
Route::view('/', 'landing');
Route::view('/panel/{any}', 'app')->where('any', '.*');

// Route::get('/api/{any}', fn() => redirect()->route('errors.404'))->where('any', '.*');
Route::view('404', 'errors.404')->name('errors.404');

Route::view('test-cal', 'index');
