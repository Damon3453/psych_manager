<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\TransformIndexRequest;
use App\Http\Controllers\Api\{
    CategoryController,
    ClientController,
    Payments\YooKassaController,
    SessionController,
    UserController,
    UserFilterController,
};
use App\Http\Controllers\Api\StaticDataController;

/*
 * Unauthorized
 */
Route::group([
    'prefix' => 'v1',
], static function (): void {
    Route::get('tooltips', [StaticDataController::class, 'getTooltips']);

    Route::group([
        'prefix' => 'payments',
        'as' => 'payments.',
    ], static function (): void {
        Route::group([
            'prefix' => 'yookassa',
            'as' => 'yookassa.',
        ], static function (): void {
            Route::post('create-payment', [YooKassaController::class, 'createPayment']);
            Route::get('payment-info/{paymentId}', [YooKassaController::class, 'getPaymentInfo']);
            Route::post('create-subscription', [YooKassaController::class, 'createSubscription']);
            Route::post('handle-subscription', [YooKassaController::class, 'handleSubscription']);
            Route::post('handle-payment-event', [YooKassaController::class, 'handlePaymentEvent']);
        });
    });
});

/*
 * Authorized
 */
Route::group([
    'prefix' => 'v1',
    'middleware' => ['auth:api', 'subscribed'],
], static function (): void {
    Route::controller(StaticDataController::class)
        ->prefix('static-data')
        ->group(static function (): void {
            Route::get('connection-types', 'getConnectionTypes');
            Route::get('meeting-types', 'getMeetingTypes');
            Route::get('gender-list', 'getGenderList');
            Route::get('categories', 'getCategories');
        });

    Route::group([
        'prefix' => 'filters',
        'middleware' => 'throttle:60,1',
    ], static function (): void {
        Route::get('{module}', [UserFilterController::class, 'get']);
        Route::post('{module}', [UserFilterController::class, 'set'])
            ->middleware([\App\Http\Middleware\ReplaceFiltersToEmptyString::class]);
        Route::get('clear/{module?}', [UserFilterController::class, 'clear']);
    });

    Route::post('restore/{id}', [ClientController::class, 'restore']);
    Route::get('session-clients', [ClientController::class, 'getSessionClients']);
    Route::get('calendar-sessions', [SessionController::class, 'getCalendarSessions']);

    Route::post('sessions/clone/{id}', [SessionController::class, 'clone'])->name('sessions.clone');
    Route::get('sessions/time-range-picker/{date?}', [SessionController::class, 'makeSessionFromToTimeRange']);

    Route::group([
        'prefix' => 'sessions/google-calendar',
        'as' => 'sessions.google-calendar.',
    ], static function (): void {
        Route::post('{session}/push', [SessionController::class, 'pushSessionToCalendar'])
            ->name('push-session-to-calendar');
    });

    Route::middleware([TransformIndexRequest::class])
        ->group(
            static fn() => Route::apiResources([
                'categories' => CategoryController::class,
                'clients' => ClientController::class,
                'sessions' => SessionController::class,
            ])
        );

    Route::controller(UserController::class)
        ->prefix('user')
        ->group(static function (): void {
            Route::get('/', 'index');
            Route::put('/', 'update');
            Route::post('sync-categories', 'syncCategories');
        });

    // Route::group([
    //     'prefix' => 'payments',
    //     'as' => 'payments.',
    // ], static function (): void {
    //     Route::group([
    //         'prefix' => 'yookassa',
    //         'as' => 'yookassa.',
    //     ], static function (): void {
    //         Route::post('create-payment', [YooKassaController::class, 'createPayment']);
    //     });
    // });
});
