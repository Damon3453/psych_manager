<?php

declare(strict_types=1);

use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerificationController;

Route::prefix('v1')->group(static function (): void {
    Route::post('auth', [OAuthController::class, 'auth'])->name('auth');
    Route::post('login', [OAuthController::class, 'issueToken'])->name('login');

    Route::get('logout', [OAuthController::class, 'logout'])->name('logout');
    Route::get('404', static fn() => response()->json(['message' => 'Wrong token or headers']))->name('404');

    //    Route::post('register', [RegisterController::class, 'register'])->name('register');
    //    Route::post('verify-email', [VerificationController::class, 'verifyEmail'])
    //        ->name('verification.verify');
    //    Route::post('verify-email/resend', [VerificationController::class, 'resend'])
    //        ->name('verification.verify.resend');
    //
    //    Route::post('verify-phone/send-sms', [VerificationController::class, 'sendSms']);
    //    Route::post('verify-phone', [VerificationController::class, 'verifyPhone'])
    //        ->name('verification.verify.phone');
});
