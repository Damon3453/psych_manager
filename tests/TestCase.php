<?php

declare(strict_types=1);

namespace Tests;

use Database\Seeders\ConnectionTypesSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    // use RefreshDatabase;

    /**
     * @var array<array-key, string>
     */
    protected array $clientScheme = [
        'id',
        'email',
        'name',
        'phone',
        'birthday_date',
        'session_id',
        'category_id',
        'category',
        'connection_type_id',
        'curator_contacts',
        'meeting_type_icon',
        'connection_type_string',
        'connection_type_link',
        'meeting_type',
        'gender',
    ];

    /**
     * @var array<array-key, string>
     */
    protected array $sessionScheme = [
        'id',
        'client_id',
        'user_id',
        'comment',
        'session_date',
        'session_time_start',
        'session_time_end',
    ];

    final public function setUp(): void
    {
        parent::setUp();
        // $this->withoutMiddleware([
        //     \Illuminate\Auth\Middleware\Authenticate::class,
        // ]);
        $this->withMiddleware(\Illuminate\Routing\Middleware\SubstituteBindings::class);
        \Artisan::call('cache:clear');
        \Artisan::call('migrate:fresh', ['--env' => 'testing']);
        $this->seed(ConnectionTypesSeeder::class);
        \Laravel\Passport\Passport::actingAs(
            \App\Models\User::factory()->has(\App\Models\Subscription::factory())->create([
                'email' => config('app.admin_email'),
            ])
        );
    }
}
