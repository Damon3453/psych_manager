<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\ClientTherapy;
use Tests\TestCase;
use App\Models\Client;
use App\Models\Session;

class SessionTest extends TestCase
{
    /**
     * @var array<string, mixed>
     */
    private array $indexFilters = [
        'options' => [
            'pagination' => [
                'page' => 1,
                'limit' => 15,
            ],
            'sort' => [
                'field' => 'session_date',
                'order' => 1,
            ],
            'fields' => [
                'id' => '',
                'user_name' => '',
                'status' => '',
                'session_date' => '',
                'date_range' => '',
                'meeting_type' => '',
                'connection_type' => '',
            ],
        ],
    ];

    public function test_index(): void
    {
        $user = auth()->user();
        Client::factory()->count(10)->afterCreating(static function (Client $client) use ($user): void {
            $client->sessions()->saveManyQuietly(
                Session::factory()
                    ->count(20)
                    ->state(static fn(array $attributes) => [
                        'user_id' => $user->id,
                        'client_id' => $client->id,
                    ])->create()
            );
        })->has(
            ClientTherapy::factory()
                ->state(static fn(array $attributes, ?Client $client) => [
                    'client_id' => $client->id,
                ]),
            'therapy'
        )->create();

        Session::factory(10)->create();
        $response = $this->getJson(route('sessions.index', [
            'options' => [
                'pagination' => [
                    'page' => 1,
                    'limit' => 15,
                ],
                'sort' => [
                    'field' => 'session_date',
                    'order' => 1,
                ],
                'fields' => [
                    'id' => '',
                    'user_name' => '',
                    'status' => '',
                    'session_date' => '',
                    'date_range' => '',
                    'meeting_type' => '',
                    'connection_type' => '',
                ],
            ],
        ]));
        $response->assertOk();
        $response->assertJsonStructure([
            'errors',
            'message',
            'data' => [
                'sessions' => [
                    array_merge($this->sessionScheme, ['client' => $this->clientScheme]),
                ],
                'total',
            ],
        ]);
    }

    public function test_store(): void
    {
        $client = Client::factory()->create();
        $date = \Carbon\Carbon::createFromTimeString('12:00');

        $response = $this->postJson(route('sessions.store'), [
            'client_id' => $client->id,
            'comment' => 'test',
            'session_date' => $date->addDay()->format('Y-m-d'),
            'session_time_start' => $date->addDay()->format('H:i'),
            'session_time_end' => $date->addDay()->format('H:i'),
        ]);

        $response->assertCreated();
    }
}
