<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Category;

class CategoryTest extends TestCase
{
    public function test_index(): void
    {
        Category::factory(5)->create();
        $response = $this->getJson(route('categories.index'));
        $response->assertOk();
    }

    public function test_store(): void
    {
        $this->assertTrue(true);
    }

    public function test_show(): void
    {
        $response = $this->getJson(route('categories.show', Category::factory()->create()));
        $response->assertOk();
    }

    public function test_update(): void
    {
        $category = Category::factory()->create();
        $response = $this
            ->putJson(route('categories.update', $category), ['category' => 'test']);
        $response->assertOk();
    }

    public function test_destroy(): void
    {
        $category = Category::factory()->create();
        $response = $this->deleteJson(route('categories.destroy', $category));
        $response->assertNoContent();

        $this->assertDatabaseMissing($category->getTable(), $category->attributesToArray());
    }
}
