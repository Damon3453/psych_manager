import * as Vue from 'vue'

import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import VueAxios from 'vue-axios'
import VueCal from 'vue-cal'
import ElementPlus from 'element-plus'
import lang from 'element-plus/lib/locale/lang/ru'
import router from './router'
import App from '@/components/layout/MainLayout.vue'

import vue3GoogleLogin from 'vue3-google-login'

import store from './store/index'

import Identity from './classes/Identity'
import axios from './classes/AxiosWrapper'
import rules from '@/components/_constants/rules'
// import Maska from 'maska/types/maska'

import '../scss/app.scss'

/**
 * @link https://antoniandre.github.io/vue-cal/
 */
import 'vue-cal/dist/vuecal.css'
import 'vue-cal/dist/i18n/ru.es.js'

/**
 * @link https://element-plus.org/en-US/guide/design.html
 */
import 'element-plus/dist/index.css'
import 'element-plus/lib'

// import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../scss/elemet-override.scss'
import '../scss/vue-cal-override.scss'

const app = Vue.createApp(App)
for (let i = 0; i < Object.entries(ElementPlusIconsVue).length; i++) {
  const [key, component] = Object.entries(ElementPlusIconsVue)[i]
  app.component(key, component)
}

app.component('VueCal', VueCal)
app
  .use(vue3GoogleLogin, {
    clientId: '463485856323-m50dhg6l7rmgeuhq1vpcb97mhn4a6up2.apps.googleusercontent.com',
  })
  .use(store)
  // .use(VueAxios, { $http: axios })
  .use(VueAxios, axios)
  // .use(axios);
// app.provide('$http', app.config.globalProperties.$http)

async function init() {
  try {
    const token = localStorage.getItem('accessToken')
    const headers = { Pragma: 'no-cache', Authorization: `Bearer ${token}` }
    const response = await axios.get('/api/v1/user', {
      withCredentials: true,
      headers,
    })
    window.$identity = new Identity(response.data.data.user)
    app.config.globalProperties.$identity = new Identity(response.data.data.user)
  } catch (e) {
    if (!e.response) {
      throw e
    }
    window.$identity = new Identity()
    app.config.globalProperties.$identity = new Identity()
  }
  app.config.globalProperties.$http = axios
  app.config.globalProperties.$localStorage = localStorage
  app.config.globalProperties.$rules = rules
  app.use(router)
  app.use(ElementPlus, { locale: lang })
  app.mount('#app')
}

init()
