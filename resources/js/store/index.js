import { createStore } from 'vuex'
// const Main = { template: './components/Main.vue' }

import staticData from '@modules/static-data.js'
import sessions from '@modules/sessions'
import userCategories from '@modules/user-categories'
import user from '@modules/user'
import client from '@modules/clientDialog'
import asideState from '@modules/asideState'

const store = createStore({
  modules: {
    staticData,
    sessions,
    userCategories,
    user,
    client,
    asideState,
  },
})

export default store
