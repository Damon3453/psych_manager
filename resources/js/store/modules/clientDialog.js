import axios from '../../classes/AxiosWrapper'

const url = '/api/v1/clients'

const state = {
  client: {},
  showClientDetails: false,
}

const getters = {
  client: (state) => state.client,
}

const actions = {
  async showClientInfo({ commit }, client) {
    const response = await axios.get(`${url}/${client.id}`)
    commit('SET_CLIENT', response.data.data.client)
    commit('SET_DETAILS', true)
  },
}

const mutations = {
  SET_DETAILS: (state, showClientDetails) => {
    state.showClientDetails = showClientDetails
  },
  SET_CLIENT: (state, client) => {
    state.client = client
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
