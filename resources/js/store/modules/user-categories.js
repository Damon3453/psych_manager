import axios from '../../classes/AxiosWrapper'

const restCategories = '/api/v1/categories'

const state = {
  categories: [],
}

const getters = {
  categories: (state) => state.categories,
}

const actions = {
  async getCategories({ commit }) {
    const response = await axios.get(restCategories)
    commit('setCategories', response.data.data.categories)
  },
  async addNewCategory(payload) {
    await axios.post(restCategories, { name: state.categories.name })
    await actions.getCategories(payload)
  },

}

const mutations = {
  setCategories: (state, categories) => {
    state.categories = categories
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
