import * as Vue from 'vue'
import App from '@/components/layout/MainLanding.vue'
// import './assets/base.css'
import 'bootstrap/dist/css/bootstrap.min.css'

const app = Vue.createApp(App)

app.mount('#app')
