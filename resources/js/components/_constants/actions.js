export const userActions = {
  rest: '/api/v1/user',
  restCategories: '/api/v1/categories',
  syncCategories: '/api/v1/user/sync-categories',
  getGenderList: '/api/v1/static-data/gender-list',
}
export const sessionActions = {
  rest: '/api/v1/sessions',
  googleCalendar: '/api/v1/sessions/google-calendar',
  restClients: '/api/v1/clients',
}
export const clientActions = {
  rest: '/api/v1/clients',
  setUserFilters: '/api/v1/filters',
  getUserFilters: '/api/v1/filters',
  clearUserFilters: '/api/v1/filters/clear',
  restore: '/api/v1/restore',
}
