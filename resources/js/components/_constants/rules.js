const RULES = {
  required:
    {
      required: true,
      message: 'Поле обязательно для заполнения',
      trigger: ['blur', 'change'],
    },
  phone: {
    pattern: /^(\+7|7|8)?[\s-]?\(?[489][0-9]{2}\)?[\s-]?[0-9]{3}[\s-]?[0-9]{2}[\s-]?[0-9]{2}$/g,
    message: 'Введите корректный номер',
    trigger: ['blur', 'change'],
  },
  email: {
    type: 'email',
    message: 'Введите корректный email',
    trigger: ['blur', 'change'],
  },
  name: {
    pattern: /([a-zа-яё0-9,.-]+[ ]*)+/ig,
    message: 'Вы указали недопустимые символы.',
    trigger: 'blur',
  },
}
export const phoneRegex = /^(\+7|7|8){1}?[\s-]?\(?[489][0-9]{2}\)?[\s-]?[0-9]{3}[\s-]?[0-9]{2}[\s-]?[0-9]{2}$/
export default RULES
