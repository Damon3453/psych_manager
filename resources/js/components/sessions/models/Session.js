import Client from '../../clients/models/Client'

export default {
  id: '',
  user_name: '',
  client_id: '',
  status: '',
  comment: '',
  session_date: '',
  session_date_copy: '',
  is_regular: false,
  repeat_period: '',
  repeat_count: 0,
  client: Client,
}
