export default {
  data() {
    return {
      actions: {
        setUserFilters: '/api/v1/filters',
        getUserFilters: '/api/v1/filters',
        clearUserFilters: '/api/v1/filters/clear',
      },
    }
  },
  created() {
    this.$root.$emit('set-path-name', { name: '' })
  },
  methods: {
    async getUserFilters(module) {
      try {
        const response = await this.$http.get(`/api/v1/filters/${module}`, {})

        return response.data.data
      } catch (e) {
        this.$message.error(e.response.data.message)
        throw e
      }
    },

    async setUserFilters(module, filters) {
      try {
        const response = await this.$http.post(`/api/v1/filters/${module}`, filters)

        return response.data.data
      } catch (e) {
        this.$message.error(e.response.data.message)
        throw e
      }
    },

    async clearUserFilters(module) {
      try {
        const response = await this.$http.get(`/api/v1/filters/clear/${module}`, {})
        this.$message.success(response.data.message)

        return response.data.data
      } catch (e) {
        this.$message.error(e.response.data.message)
        throw e
      }
    },
  },
}
