import { createRouter, createWebHistory } from 'vue-router'

// import NotFound from '@layout/NotFound.vue'

import DefaultHeader from '@layout/Header.vue'
import DefaultAside from '@layout/MainAside.vue'

import Login from '@auth/Login.vue'
import Register from '@auth/Register.vue'
import SuccessConfirmation from '@auth/SuccessConfirmation.vue'
import GoogleProvider from '@auth/GoogleProvider.vue'
// import LoginAside from "@auth/LoginAside.vue"
// import LoginHeader from "@auth/LoginHeader.vue"

import Clients from '@clients/ClientsIndex.vue'
import ClientForm from '@clients/ClientForm.vue'

import Sessions from '@sessions/SessionIndex.vue'
import SessionsForm from  '@sessions/SessionForm.vue'
import Main from '@/components/MainPage.vue'

import Profile from  '@/components/profile/ProfileForm.vue'

const routes = [
  // {
  //   path: '/:pathMatch(.*)*',
  //   name: 'not-found',
  //   components: {
  //     default: NotFound
  //   }
  // },
  {
    path: '',
    name: 'main',
    components: { aside: DefaultAside, header: DefaultHeader, default: Main },
    meta: { auth: true, menuitem: '0' },
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: { auth: false, menuitem: '1' },
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: { auth: false, menuitem: '1-1' },
  },
  {
    path: '/confirm/:hash',
    name: 'confirm',
    component: SuccessConfirmation,
    meta: { auth: false, menuitem: '1-2' },
  },
  {
    path: '/clients',
    name: 'clients',
    components: { aside: DefaultAside, header: DefaultHeader, default: Clients },
    meta: { auth: true, menuitem: '2' },
  },
  {
    path: '/sessions',
    name: 'sessions',
    components: { aside: DefaultAside, header: DefaultHeader, default: Sessions },
    meta: { auth: true, menuitem: '3' },
  },
  {
    path: '/sessions/:id',
    name: 'sessions.update',
    components: { aside: DefaultAside, header: DefaultHeader, default: SessionsForm },
    meta: { auth: true, menuitem: '4-3' },
  },
  {
    path: '/sessions/create',
    name: 'sessions.create',
    components: { aside: DefaultAside, header: DefaultHeader, default: SessionsForm },
    meta: { auth: true, menuitem: '4-3' },
  },
  {
    path: '/clients/:id',
    name: 'clients.update',
    components: { aside: DefaultAside, header: DefaultHeader, default: ClientForm },
    meta: { auth: true, menuitem: '4-4' },
  },
  {
    path: '/clients/create',
    name: 'clients.create',
    components: { aside: DefaultAside, header: DefaultHeader, default: ClientForm },
    meta: { auth: true, menuitem: '4-2' },
  },
  {
    path: '/profile',
    name: 'profile',
    components: { aside: DefaultAside, header: DefaultHeader, default: Profile },
    meta: { auth: true, menuitem: '4-5' },
  },
  {
    path: '/google',
    name: 'google',
    component: GoogleProvider,
  },
]

const router = createRouter({
  history: createWebHistory('/panel'),
  routes,
})
// router.resolve({
//   name: 'not-found',
//   params: { pathMatch: ['not', 'found'] },
// }).href

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.auth)) {
    if (!window.$identity.isGuest) {
      next()
      return
    }
    next({
      path: '/login',
      query: { redirect: to.fullPath },
    })
  } else {
    next()
  }
})

// router.beforeEach((to, from, next)=>{
//   if ( to.name !== 'login' && !window.$identity.isGuest) {
//     next({
//       path: 'login',
//       replace: true
//     })
//   } else {
//     next();
//   }
// })

//  Fix error NavigationDuplicated second option
const originalPush = createRouter.push
// eslint-disable-next-line func-names
createRouter.push = function push(location) {
  return originalPush.call(this, location).catch((err) => {
    if (err.name !== 'NavigationDuplicated') throw err
  })
}

export default router
