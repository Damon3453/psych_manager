<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('images/logo.png') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600;700&display=swap">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <title>Psych Manager</title>
</head>

<body>
    <div style="height:100vh; width: 100vw;" class="d-flex justify-content-center align-items-center">
        <div>
            @if(!empty($data))
                @dd($data)
            @endif
        </div>
        <div>
            <form method="post" action="{{ route('post.alerts') }}">
                @csrf
                <button class="btn btn-primary">
                    Submit
                </button>
            </form>
            <a
{{--                href="https://www.donationalerts.com/oauth/authorize?client_id=10183&redirect_uri=http://psych-manager.local/donation-alerts&response_type=code&scope=oauth-donation-index"--}}
                href="{{ route('post.alerts') }}"
                class="btn btn-primary"
            >
                Submit
            </a>
        </div>
    </div>
</body>
</html>
