<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Cache\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rules\Password;
use Opcodes\LogViewer\Facades\LogViewer;
use App\Notifications\DiscordNotification;
use App\Contracts\PaymentContract;
use App\Services\Cache\CacheManager;
use App\Services\Payments\PaymentManager;
use App\Services\SmsAeroService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        \Carbon\Carbon::setLocale(config('app.locale'));
        $this->app->bind(Store::class, CacheManager::class);
        $this->app->singleton(
            DiscordNotification::class,
            static fn() => new DiscordNotification(config('services.discord.errors_webhook'))
        );
        $this->app->singleton(
            SmsAeroService::class,
            static fn() => new SmsAeroService(
                config('services.sms_aero.login'),
                config('services.sms_aero.api_key')
            )
        );

        $this->app->singleton(PaymentContract::class, static function ($app) {
            return new PaymentManager($app);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @param UrlGenerator $url
     *
     * @return void
     */
    public function boot(UrlGenerator $url): void
    {
        if (!$this->app->isLocal() && !$this->app->runningUnitTests()) {
            $url->forceScheme('https');
        }
        if (!$this->app->isProduction() && !$this->app->runningInConsole()) {
            \DB::listen(function (\Illuminate\Database\Events\QueryExecuted $query) {
                if ($query->time > 150) {
                    $this->app->get(DiscordNotification::class)
                        ->setFields([
                            ['name' => 'Message', 'value' => 'query longer than 1,5s'],
                            ['name' => 'Sql', 'value' => $query->sql],
                            ['name' => 'Bindings', 'value' => $query->bindings],
                        ])->pushEmbed()->toDiscord();
                }
            });
        }

        // LogViewer::auth(
        //     static fn($request) => $request->user() instanceof \App\Models\User &&
        //         $request->user()->email === config('app.admin_email')
        // );
        LogViewer::auth(fn(\Illuminate\Http\Request $request) => $this->app->isLocal());

        // Model::shouldBeStrict($this->app->isLocal());
        Model::preventLazyLoading($this->app->isLocal());
        // Model::preventSilentlyDiscardingAttributes($this->app->isLocal());
        // Model::preventAccessingMissingAttributes($this->app->isLocal());

        Password::defaults(
            static fn() =>
                Password::min(12)
                    ->letters()
                    ->numbers()
                    ->symbols()
                    ->mixedCase()
                    ->uncompromised()
        );
        JsonResource::withoutWrapping();
    }
}
