<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\AuthorizationServer;
use App\Grants\PhoneGrant;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();
        $now = \Carbon\Carbon::now();

        Passport::tokensExpireIn($now->addDays(14));
        Passport::refreshTokensExpireIn($now->addDays(30));
        Passport::personalAccessTokensExpireIn($now->addMonths(6));

        if (!$this->app->runningInConsole()) {
            app(AuthorizationServer::class)->enableGrantType($this->makePhoneGrant(), Passport::tokensExpireIn());
        }

        // Passport::routes(null, ['prefix' => 'api/v1/oauth']);
    }

    /**
     * Create and configure a Phone grant instance.
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     *
     * @return PhoneGrant
     */
    protected function makePhoneGrant(): PhoneGrant
    {
        $grant = new PhoneGrant(
            $this->app->make(AccessTokenRepository::class),
            $this->app->make(RefreshTokenRepository::class),
        );
        $passport = Passport::refreshTokensExpireIn(\Carbon\Carbon::now()->addDays(30));

        // Get the expiration duration for refresh tokens
        $refreshTokenExpiration = $passport::$refreshTokensExpireIn;

        // Parse the string duration to obtain the duration and unit
        // [$duration, $unit] = sscanf($refreshTokenExpiration, '%d %s');

        // Create a CarbonInterval object based on the duration and unit
        // $refreshTokenTTL = \Carbon\CarbonInterval::make($refreshTokenExpiration, '%d %s');

        // Get the expiration duration for refresh tokens
        // $refreshTokenExpiration = Passport::refreshTokensExpireIn(now()->addDays(30));

        // Calculate the expiration timestamp
        // $expirationTimestamp = now()->add($refreshTokenExpiration)->getTimestamp();

        // Get the current timestamp
        // $currentTimestamp = now()->getTimestamp();

        // Calculate the difference between expiration and current timestamps
        // $timestampDifference = $expirationTimestamp - $currentTimestamp;

        // Create a DateInterval object with the difference in seconds
        // $refreshTokenTTL = new \DateInterval('PT' . $timestampDifference . 'S');

        $grant->setRefreshTokenTTL($refreshTokenExpiration);

        return $grant;
    }
}
