<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class CarbonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        Carbon::macro('makeFromToTimeRange', static function (int $from = 7, int $to = 22): Collection {
            $start = Carbon::create(hour: $from);
            $end = Carbon::create(hour: $to);
            $result = collect()->push([
                'value' => "0$from:00",
                'disabled' => false,
            ]);
            while ($start->lt($end)) {
                $result->push([
                    'value' => $start->addMinutes(10)->format('H:i'),
                    'disabled' => false,
                ]);
            }

            return $result;
        });
    }
}
