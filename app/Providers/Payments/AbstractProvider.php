<?php

declare(strict_types=1);

namespace App\Providers\Payments;


abstract class AbstractProvider
{
    /**
     * @var \App\Models\Subscription
     */
    protected \Illuminate\Database\Eloquent\Model $subscription;

    /**
     * The payment token.
     *
     * @var null|string
     */
    public ?string $token = null;

    /**
     * @param \App\Models\Subscription $subscription
     *
     * @return $this
     */
    public function subscription(\Illuminate\Database\Eloquent\Model $subscription): static
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Set the payment token.
     *
     * @param string $token
     *
     * @return $this
     */
    public function token(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Create a charge for a payment token.
     *
     * @return \Spatie\LaravelData\Contracts\DataObject|null
     */
    abstract public function charge(): \Spatie\LaravelData\Contracts\DataObject|null;
}
