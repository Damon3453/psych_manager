<?php

declare(strict_types=1);

namespace App\Providers\Payments;

use App\DataObjects\Payments\YooKassa\YooKassaErrorResponseData;
use App\DataObjects\Payments\YooKassa\YooKassaSuccessResponseData;
use YooKassa\Client;

class YooKassa extends AbstractProvider
{
    public Client $yooClient;

    /**
     * @var YooKassaSuccessResponseData
     */
    public \Spatie\LaravelData\Contracts\DataObject $response;

    public function __construct()
    {
        $this->yooClient = (new Client())->setAuth(
            config('services.yookassa.shop_id'),
            config('services.yookassa.api_key')
        );
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     *
     * @param string $paymentId
     *
     * @return YooKassaSuccessResponseData|null
     */
    public function getPaymentInfo(string $paymentId): \Spatie\LaravelData\Contracts\DataObject|null
    {
        $response = $this->yooClient->getPaymentInfo($paymentId);

        if (in_array($response->status, [400, 401, 403, 404, 429, 500], true)) {
            return YooKassaErrorResponseData::from($response->toArray());
        }
        if (in_array($response->status, [405, 415], true)) {
            // Reason-Phrase header
            return YooKassaErrorResponseData::optional();
        }

        return YooKassaSuccessResponseData::from($response->toArray());
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     *
     * @return YooKassaSuccessResponseData|null
     */
    public function createPayment(): \Spatie\LaravelData\Contracts\DataObject|null
    {
        $response = $this->yooClient->createPayment([
            'amount' => [
                'value' => 100.0,
                'currency' => 'RUB',
            ],
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => config('app.url') . '/return_url',
            ],
            'capture' => true,
            'description' => 'Test order 1',
        ]);

        if (in_array($response->status, [400, 401, 403, 404, 429, 500], true)) {
            return YooKassaErrorResponseData::from($response->toArray());
        }
        if (in_array($response->status, [405, 415], true)) {
            // Reason-Phrase header
            return YooKassaErrorResponseData::optional();
        }

        $this->response = YooKassaSuccessResponseData::from($response->toArray());

        return $this->response;
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     *
     * @return YooKassaSuccessResponseData|null
     */
    public function charge(): \Spatie\LaravelData\Contracts\DataObject|null
    {
        $response = $this->yooClient->createPayment([
            'amount' => [
                'value' => 1000.0,
                'currency' => 'RUB',
            ],
            'payment_method_data' => [
                'type' => 'bank_card',
            ],
            'confirmation' => [
                'type' => 'redirect',
                // todo:
                'return_url' => config('app.url') . '/panel',
            ],
            'capture' => true,
            // todo:
            'description' => 'Test order 12',
            'save_payment_method' => true,
        ]);

        if (in_array($response->status, [400, 401, 403, 404, 429, 500], true)) {
            return YooKassaErrorResponseData::from($response->toArray());
        }
        if (in_array($response->status, [405, 415], true)) {
            // Reason-Phrase header
            return YooKassaErrorResponseData::optional();
        }

        $this->response = YooKassaSuccessResponseData::from($response->toArray());

        return $this->response;
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     *
     * @return YooKassaSuccessResponseData|null
     */
    public function handleSubscription(): \Spatie\LaravelData\Contracts\DataObject|null
    {
        $response = $this->yooClient->createPayment([
            'amount' => [
                'value' => $this->subscription->amount,
                'currency' => 'RUB',
            ],
            'capture' => true,
            'payment_method_id' => $this->subscription->payment_method_id,
        ]);

        if (in_array($response->status, [400, 401, 403, 404, 429, 500], true)) {
            return YooKassaErrorResponseData::from($response->toArray());
        }
        if (in_array($response->status, [405, 415], true)) {
            // Reason-Phrase header
            return YooKassaErrorResponseData::optional();
        }

        return YooKassaSuccessResponseData::from($response->toArray());
    }

    public function cancelSubscription(): void
    {
        $this->subscription->update([
            'status' => 'canceled',
        ]);

        // notify yookassa?
    }
}
