<?php

declare(strict_types=1);

namespace App\Services\Payments;

use Illuminate\Support\Manager;
use App\Contracts\PaymentContract;
use App\Providers\Payments;

class PaymentManager extends Manager implements PaymentContract
{
    /**
     * Build a layout provider instance.
     *
     * @param string $provider
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     *
     * @return Payments\Tinkoff|Payments\YooKassa
     */
    private function buildProvider(string $provider): Payments\AbstractProvider
    {
        return $this->container->make($provider);
    }

    /**
     * @inheritdoc
     */
    public function from(string $driver): Payments\AbstractProvider
    {
        return $this->driver($driver);
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function createYooKassaDriver(): Payments\YooKassa
    {
        return $this->buildProvider(Payments\YooKassa::class);
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function createTinkoffDriver(): Payments\Tinkoff
    {
        return $this->buildProvider(Payments\Tinkoff::class);
    }

    /**
     * @inheritdoc
     */
    public function getDefaultDriver(): string
    {
        return 'YooKassa';
    }
}
