<?php

declare(strict_types=1);

namespace App\Services\Core;

use App\Contracts\{CommonDtoContract, ServiceModelContract};

class CategoryRequestModel implements ServiceModelContract
{
    /**
     * @var \Illuminate\Support\Collection<array-key, mixed>
     */
    public \Illuminate\Support\Collection $categories;

    /**
     * @var \Illuminate\Support\Collection<array-key, mixed>
     */
    public \Illuminate\Support\Collection $newCategories;

    public function __construct(CommonDtoContract $dto = null)
    {
        if ($dto) {
            $this->loadFromDto($dto);
        }
    }

    /**
     * @param CommonDtoContract $dto
     *
     * @return $this
     */
    public function loadFromDto(CommonDtoContract $dto): static
    {
        /* @var \App\Dto\CategoryRequestDto $dto */
        $this->categories = $dto->categories;
        $this->newCategories = $dto->newCategories;

        return $this;
    }
}
