<?php

declare(strict_types=1);

namespace App\Services\App;

use Psr\Http\Message\ServerRequestInterface;

final readonly class Client
{
    private object $dbClient;

    public function __construct(
        private ServerRequestInterface $request
    ) {
        $this->dbClient = \DB::table('oauth_clients')->where([
            ['password_client', true],
            ['personal_access_client', false],
        ])->first();
    }

    private function client(): ?ServerRequestInterface
    {
        return $this->request->withParsedBody([
            'client_id' => $this->dbClient->id,
            'client_secret' => $this->dbClient->secret,
            'grant_type' => $this->request->getParsedBody()['grant_type'] ?? 'phone',
            'phone' => $this->request->getParsedBody()['phone'],
            'code' => $this->request->getParsedBody()['code'],
        ]);
    }

    public function parseRequestBody(): ?ServerRequestInterface
    {
        return $this->client();
    }

    public function getPhone(): string
    {
        return $this->request->getParsedBody()['phone'];
    }

    public function getGrantType(): string
    {
        return $this->request->getParsedBody()['grant_type'];
    }
}
