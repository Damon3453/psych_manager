<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Contracts\Cache\Store;
use App\Contracts\{CommonFactoryContract, RepositoryContract};

abstract class BaseService
{
    protected CommonFactoryContract $factory;

    protected string $model;

    protected string $repositoryClass;

    protected RepositoryContract $repository;

    public function __construct(public Store $cache)
    {
        $this->repository = $this->getRepositoryEntity();
    }

    final public function cacheKey(): string
    {
        return $this->getModel()->getTable() . '_' . debug_backtrace()[1]['function'];
    }

    /**
     * @param string $key
     *
     * @throws \Throwable
     *
     * @return mixed
     */
    final public function getByKey(string $key): mixed
    {
        return $this->cache->get($key);
    }

    /**
     * @return \App\Contracts\RepositoryContract
     */
    final public function getRepositoryEntity(): RepositoryContract
    {
        return app($this->repositoryClass);
    }

    /**
     * @return \App\Models\BaseModel
     */
    final public function getModel(): \App\Models\BaseModel
    {
        return app($this->model);
    }

    final public function getFactory(): CommonFactoryContract
    {
        return $this->factory;
    }
}
