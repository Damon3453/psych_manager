<?php

declare(strict_types=1);

namespace App\Services;

use SmsAero\SmsAeroMessage;
use App\DataValueObjects\PhoneValue;

class SmsAeroService extends SmsAeroMessage
{
    /**
     * @param PhoneValue $phone
     * @param string $text
     *
     * @throws \Exception
     *
     * @return array
     */
    public function testSend(PhoneValue $phone, string $text): array
    {
        return $this->makeRequest('POST', 'sms/testsend', [
            'number' => $phone->toNative(),
            'text' => $text,
            'sign' => 'SMS Aero',
        ]);
    }

    /**
     * @param \App\DataValueObjects\PhoneValue $phone
     * @param string $text
     *
     * @throws \Exception
     *
     * @return array
     */
    public function handle(PhoneValue $phone, string $text): array
    {
        return $this->send([
            'number' => $phone->toNative(),
            'text' => $text,
            'sign' => 'SMS Aero',
        ]);
    }
}
