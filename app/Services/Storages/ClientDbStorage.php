<?php

declare(strict_types=1);

namespace App\Services\Storages;

use App\Factories\ClientFactory;
use App\Contracts\{CommonFactoryContract, ServiceModelContract, StorageContract};
use App\Models\{
    Client, Category, ClientTherapy, ConnectionType
};

class ClientDbStorage implements StorageContract
{
    protected CommonFactoryContract $factory;

    public function __construct()
    {
        $this->factory = new ClientFactory();
    }

    /**
     * @param CommonFactoryContract $factory
     *
     * @return $this
     */
    public function setFactory(CommonFactoryContract $factory): static
    {
        $this->factory = $factory;

        return $this;
    }

    /**
     * @param \App\Services\Core\ClientModel $client
     *
     * @throws \Throwable
     *
     * @return Client
     */
    public function save(ServiceModelContract $client): Client
    {
        if ($client->id) {
            $newModel = Client::query()->find($client->id);
            if ($client->email === $newModel->email) {
                $client->email = null;
            }
            $newModel->update((array) $client);
        } else {
            $newModel = new Client((array) $client);
        }

        $connectionType = ConnectionType::find($client->connection_type_id);
        $category = Category::find($client->category_id);

        $newModel->user()->associate($client->user_id);
        $newModel->category()->associate($category);

        if (!$connectionType->clients()->save($newModel)) {
            throw new \App\Exceptions\EntityStoreException('Ошибка сохранения клиента');
        }

        $therapy = ClientTherapy::n()
            ->setProblemSeverity($client->problem_severity)
            ->setPlan($client->plan)
            ->setRequest($client->request)
            ->setNotes($client->notes)
            ->setConceptVision($client->concept_vision);

        $therapy->client()->associate($newModel);

        if (!$therapy->save()) {
            throw new \App\Exceptions\EntityStoreException('Ошибка создания данных терапии');
        }

        return $newModel;
    }
}
