<?php

declare(strict_types=1);

namespace App\Services\Cache;

use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Cache\Store;

/**
 * @implements Store
 *
 * @property \Illuminate\Contracts\Cache\Store $store
 * @property int|null $default
 *
 * Base methods:
 *
 * @method bool has(string $key)
 * @method bool missing(string $key)
 * @method iterable getMultiple($keys, $default = null)
 * @method mixed pull(string $key, $default = null)
 * @method bool set(string $key, mixed $value, \DateInterval|int|null $ttl = null)
 * @method bool setMultiple(iterable $values, \DateInterval|int|null $ttl = null)
 * @method mixed remember(string $key, \Closure|\DateInterval|\DateTimeInterface|int|null $ttl, \Closure $callback)
 * @method mixed sear(string $key, \Closure $callback)
 * @method mixed rememberForever(string $key, \Closure $callback)
 * @method bool delete(string $key)
 * @method bool deleteMultiple(iterable $keys)
 * @method bool clear()
 * @method bool supportsTags()
 * @method int|null getDefaultCacheTime()
 * @method $this setDefaultCacheTime(int|null $seconds)
 *
 * Private base methods:
 * @method mixed handleManyResult(array $keys, string $key, mixed $value)
 * @method bool putManyForever(array $values)
 * @method string itemKey(string $key)
 * @method int getSeconds(\DateInterval|\DateTimeInterface|int $ttl)
 */
class CacheManager implements Store
{
    /**
     * The default number of seconds to store items.
     *
     * @var int|null
     */
    protected ?int $defaultTtl = 3600;

    public static function make(): static
    {
        return new static();
    }

    /**
     * @inheritdoc
     */
    public function many($keys): array
    {
        return Cache::store('redis')->many($keys);
    }

    public function add($key, $value): bool
    {
        return Cache::store('redis')->add($key, $value, $this->defaultTtl);
    }

    /**
     * @param array|mixed $names
     *
     * @return \Illuminate\Cache\TaggedCache
     */
    public function tags(mixed $names): \Illuminate\Cache\TaggedCache
    {
        return Cache::store('redis')->tags($names);
    }

    /**
     * @inheritdoc
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function get($key, $default = null): mixed
    {
        return Cache::store('redis')->get($key, $default);
    }

    /**
     * @inheritdoc
     */
    public function put($key, $value, $seconds = null): bool
    {
        return Cache::store('redis')->put($key, $value, $this->defaultTtl);
    }

    /**
     * @inheritdoc
     */
    public function putMany(array $values, $seconds = null): bool
    {
        return Cache::store('redis')->putMany($values, $this->defaultTtl);
    }

    /**
     * @inheritdoc
     */
    public function increment($key, $value = 1): int|bool
    {
        return Cache::store('redis')->increment($key, $value);
    }

    /**
     * @inheritdoc
     */
    public function decrement($key, $value = 1): int|bool
    {
        return Cache::store('redis')->decrement($key, $value);
    }

    /**
     * @inheritdoc
     */
    public function forever($key, $value): bool
    {
        return Cache::store('redis')->forever($key, $value);
    }

    /**
     * @inheritdoc
     */
    public function forget($key): bool
    {
        return Cache::store('redis')->forget($key);
    }

    /**
     * @inheritdoc
     */
    public function flush(): bool
    {
        return Cache::store('redis')->flush();
    }

    /**
     * @inheritdoc
     */
    public function getPrefix(): string
    {
        return Cache::getPrefix();
    }
}
