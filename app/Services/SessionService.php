<?php

declare(strict_types=1);

namespace App\Services;

use Carbon\CarbonInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;
use App\Repositories\SessionRepository;
use App\Models\{Session, User, Client};

class SessionService extends BaseService
{
    /**
     * @var \Illuminate\Support\Collection<array-key, mixed>
     */
    public Collection $data;

    public User $user;

    protected string $model = Session::class;

    protected string $repositoryClass = SessionRepository::class;

    /**
     * @var SessionRepository
     */
    protected \App\Contracts\RepositoryContract $repository;

    /**
     * @param \Carbon\CarbonInterface|null $carbonCurrentTime
     * @param \Carbon\CarbonInterface|null $carbonSessionTimeEnd
     * @param \Carbon\CarbonInterface|null $carbonSessionTimeStart
     *
     * @return bool
     */
    private function checkSessionAvailableTime(
        ?CarbonInterface $carbonCurrentTime,
        ?CarbonInterface $carbonSessionTimeEnd,
        ?CarbonInterface $carbonSessionTimeStart
    ): bool {
        return $carbonCurrentTime?->lte($carbonSessionTimeEnd) && $carbonCurrentTime?->gte($carbonSessionTimeStart);
    }

    /**
     * @param \Illuminate\Support\Collection $data
     *
     * @return $this
     */
    public function setData(Collection $data): static
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param \App\Models\User $user
     *
     * @return $this
     */
    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param array $filters
     * @param array|null $total
     * @param User $user
     *
     * @throws \Throwable
     *
     * @return Collection
     */
    public function getSessionsWithFilters(
        array $filters,
        ?array &$total,
        \Illuminate\Foundation\Auth\User $user
    ): Collection {
        if (
            $this->getByKey($this->cacheKey())?->isNotEmpty() &&
            $this->getByKey("{$this->cacheKey()}_filters") === $filters
        ) {
            if ($this->getByKey($this->cacheKey()) instanceof Collection) {
                $total = $this->getByKey($this->cacheKey())->count();
            } else {
                $total = count($total);
            }

            return $this->getByKey($this->cacheKey());
        }
        $sessionsQ = $this->repository->filtered($filters, $total, $user);
        $this->cache->put(
            $this->cacheKey(),
            $sessions = $sessionsQ->get()
        );
        $this->cache->put(
            "{$this->cacheKey()}_filters",
            $filters
        );

        return $sessions;
    }

    /**
     * @throws \Throwable
     *
     * @return array|Collection<Session>
     */
    public function getLastMonthSessions(): array|Collection
    {
        $date = Carbon::now();

        if ($this->getByKey($this->cacheKey())?->isNotEmpty()) {
            return $this->getByKey($this->cacheKey());
        }

        $this->cache->put(
            $this->cacheKey(),
            $sessions = $this->repository->calendar($date)->get()
        );

        return $sessions;
    }

    /**
     * @param \App\Models\User $user
     * @param int|null $id
     *
     * @throws \Throwable
     *
     * @return void
     */
    public function updateOrCreate(User $user, int $id = null): void
    {
        $session = Session::find($id);
        if (!$session) {
            $session = Session::n();
        }
        $session->setComment($this->data->get('comment'))
            ->setSessionDate($this->data->get('session_date'))
            ->setSessionTimeStart($this->data->get('session_time_start'))
            ->setSessionTimeEnd($this->data->get('session_time_end'));
        $client = Client::find($this->data->get('client_id'));
        if (!$client) {
            throw new \App\Exceptions\EntityNotFoundException('Клиент не найден');
        }
        if (!$session->client()->associate($client) || !$session->user()->associate($user)->save()) {
            throw new \App\Exceptions\EntityStoreException('Ошибка создания');
        }

        if ($this->isRegularSession()) {
            $repeatingPeriod = $this->resolveRepeatingPeriodAsCarbonMethod();
            for ($i = 1; $i <= $this->data->get('repeat_count', 0); $i++) {
                /* @var Session $newRecord */
                if ($i === 1) {
                    $newRecord = $session->client()->associate($client);
                    $newRecord->duplicate()->touch();
                    $session->user()->associate($user)->save();
                } else {
                    $newRecord = $newRecord->client()->associate($client);
                    $newRecord->duplicate()->touch();
                    $newRecord->user()->associate($user)->save();
                }
                /** @var Carbon $date */
                $date = $newRecord->session_date->$repeatingPeriod();
                $newRecord->setSessionDate($date->format('Y-m-d H:i:s'));
                $newRecord->save();
            }
        }
    }

    /**
     * @return string
     */
    public function resolveRepeatingPeriodAsCarbonMethod(): string
    {
        return match ($this->data->get('repeat_period')) {
            'День' => 'addDay',
            'Неделя' => 'addWeek',
            'Месяц' => 'addMonth',
        };
    }

    /**
     * @return bool
     */
    public function isRegularSession(): bool
    {
        return $this->data->get('is_regular', false)
            && $this->data->get('repeat_period')
            && $this->data->get('repeat_count', 0);
    }

    /**
     * @param string|null $date
     *
     * @return \Illuminate\Support\Collection
     */
    public function getTodaySessions(string $date = null): Collection
    {
        $allTime = Carbon::makeFromToTimeRange();
        /** @var Collection<Session> $currentSessions */
        $currentSessions = $this->repository
            ->getTodaySessions(Carbon::parse($date), $this->user)
            ->get();
        if ($currentSessions->isEmpty()) {
            return $allTime;
        }

        $carbonSessionTimeStart = null;
        $carbonSessionTimeEnd = null;

        return $allTime->map(function ($item) use ($currentSessions, &$carbonSessionTimeStart, &$carbonSessionTimeEnd) {
            /** @var Session $currentSession */
            $carbonCurrentTime = Carbon::createFromTimestamp(strtotime($item['value']));
            $currentSession = $currentSessions->firstWhere('session_time_start', $carbonCurrentTime);
            if ($currentSession) {
                $carbonSessionTimeStart = $currentSession->session_time_start;
                $carbonSessionTimeEnd = $currentSession->session_time_end;
                $item['disabled'] = $currentSession->session_time_start === $item['value'] ||
                    (
                        $carbonSessionTimeStart->lte($carbonCurrentTime) &&
                        $carbonSessionTimeEnd->gte($carbonCurrentTime)
                    );
            }
            if (!$currentSession) {
                $item['disabled'] = $this->checkSessionAvailableTime(
                    $carbonCurrentTime,
                    $carbonSessionTimeEnd,
                    $carbonSessionTimeStart
                );
            }

            return $item;
        });
    }
}
