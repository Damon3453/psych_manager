<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Collection;
use App\Contracts\ServiceModelContract;
use App\Factories\CategoryRequestFactory;
use App\DataValueObjects\PhoneValue;
use App\Models\{Category, UserPhoneVerify};

class UserService extends BaseService
{
    /**
     * @var \App\Services\Core\CategoryRequestModel
     */
    protected ServiceModelContract $data;

    protected string $category;

    protected Collection $input;

    /**
     * @var \App\Models\User
     */
    protected \Illuminate\Foundation\Auth\User $user;

    public function __construct()
    {
        $this->factory = new CategoryRequestFactory();
        // $this->storage = new ClientDbStorage();
        // $this->storage->setFactory($this->factory);
    }

    // public function setData(ServiceModelContract $coreModel): static
    // {
    //     $this->data = $coreModel;
    //
    //     return $this;
    // }

    public function setCategory(string $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function setUser(\Illuminate\Foundation\Auth\User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function setInput(Collection $input): static
    {
        $this->input = $input;

        return $this;
    }

    /**
     * @return Category
     */
    public function saveCategory(): Category
    {
        return $this->user->categories()->updateOrCreate(['name' => $this->category]);
    }

    /**
     * @return void
     */
    public function verifyPhone(): void
    {
        $phone = PhoneValue::make($this->user->phone);

        $userPhoneVerify = $this->user->userPhoneVerifies()->where([
            'is_used' => false,
            'phone' => $phone->toNative(),
        ])->latest()->firstOrFail();

        if (!\App::isLocal()) {
            if ($userPhoneVerify->code !== $this->input->get('code') || !$userPhoneVerify->isValid()) {
                throw \App\Exceptions\InvalidParameterException::make('Неверный код.');
            }
        }
        $userPhoneVerify->markCodeAsUsed();

        $this->user->userPhoneVerifies()->where('is_used', false)->delete();

        // UserPhoneVerify::query()->where('is_used', false)->delete();

        $this->user->markPhoneAsVerified();
    }
}
