<?php

declare(strict_types=1);

namespace App\DataObjects\Filters;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class SessionFilterData extends Data
{
    public function __construct(
        public ?string $id = '',
        public ?string $userName = '',
        public ?string $status = '',
        public ?string $sessionDate = '',
        public ?array $dateRange = [],
        public ?string $meetingType = '',
        public ?int $connectionType = null,
    ) {
    }
}
