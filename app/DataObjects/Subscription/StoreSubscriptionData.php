<?php

declare(strict_types=1);

namespace App\DataObjects\Subscription;

use Carbon\Carbon;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class StoreSubscriptionData extends Data
{
    /**
     * @var \Carbon\Carbon
     */
    public \Carbon\CarbonInterface $cancelDate;

    public function __construct(
        public ?string $paymentMethodId,
        public string $status,
        public int $period = 1,
        public string $amount = '1000',
        public ?Data $response = null
    ) {
        $now = Carbon::now();
        $this->cancelDate = $this->period === 0 ?
            $now->addWeeks(2) :
            $now->addMonths($this->period);
    }
}
