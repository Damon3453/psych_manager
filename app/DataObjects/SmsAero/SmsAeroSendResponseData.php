<?php

declare(strict_types=1);

namespace App\DataObjects\SmsAero;

use Spatie\LaravelData\Data;

class SmsAeroSendResponseData extends Data
{
    public function __construct(
        public int $id,
        public string $from,
        public int $number,
        public string $text,
        public int $status,
        public string $extendStatus,
        public string $channel,
        public string $cost,
        public int $dateCreate,
        public int $dateSend,
    ) {
    }
}
