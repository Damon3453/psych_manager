<?php

declare(strict_types=1);

namespace App\DataObjects\User;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use App\DataValueObjects\PhoneValue;

#[MapName(SnakeCaseMapper::class)]
class UserRegisterData extends Data
{
    public function __construct(
        public string $phone,
        public ?string $name = null,
        public ?string $email = null,
    ) {
        $this->phone = PhoneValue::make($phone)->toNative();
    }
}
