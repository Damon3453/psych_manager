<?php

declare(strict_types=1);

namespace App\DataObjects\Payments\YooKassa\Notification;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use App\DataObjects\Payments\YooKassa\PaymentMethod\YooKassaPaymentMethodResponseData;

#[MapName(SnakeCaseMapper::class)]
class YooKassaPaymentStatusData extends Data
{
    public function __construct(
        public string $id,
        public string $status,
        public string $description,
        public string $createdAt,
        public bool $paid,
        public bool $refundable,
        public ?string $capturedAt = null,
        public array $authorizationDetails = [
            'rrn' => '',
            'auth_code' => '',
            'three_d_secure' => [
                'applied' => false,
                'method_completed' => false,
                'challenge_completed' => false,
            ],
        ],
        public array $metadata = [],
        public array $amount = ['value' => '0.00', 'currency' => 'RUB'],
        public array $incomeAmount = ['value' => '0.00', 'currency' => 'RUB'],
        public array $recipient = ['account_id' => '', 'gateway_id' => ''],
        public ?YooKassaPaymentMethodResponseData $paymentMethod = null,
        public array $refundedAmount = ['value' => '0.00', 'currency' => 'RUB'],
        public ?array $cancellationDetails = null,
    ) {
    }
}
