<?php

declare(strict_types=1);

namespace App\DataObjects\Payments\YooKassa;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Spatie\LaravelData\Optional;

#[MapName(SnakeCaseMapper::class)]
class YooKassaErrorResponseData extends Data
{
    public function __construct(
        public string|Optional $id,
        public string|Optional $type,
        public string|Optional $code,
        public string|Optional $description,
        public string|Optional $parameter
    ) {
    }
}
