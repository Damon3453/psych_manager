<?php

declare(strict_types=1);

namespace App\DataObjects\Payments\YooKassa;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use App\DataObjects\Payments\YooKassa\PaymentMethod\YooKassaPaymentMethodResponseData;

#[MapName(SnakeCaseMapper::class)]
class YooKassaSuccessResponseData extends Data
{
    public function __construct(
        public string $id,
        public string $status,
        public bool $paid,
        public string $createdAt,
        public string $description,
        public array $amount = ['value' => '0.00', 'currency' => 'RUB'],
        public array $confirmation = ['type' => 'redirect', 'confirmation_url' => ''],
        public array $metadata = [],
        public array $recipient = ['account_id' => '', 'gateway_id' => ''],
        public bool $refundable = false,
        public bool $test = false,
        public ?YooKassaPaymentMethodResponseData $paymentMethod = null,
        public ?string $expiresAt = null,
        public array $cancellationDetails = ['party' => '', 'reason' => ''],
    ) {
    }
}
