<?php

declare(strict_types=1);

namespace App\DataObjects\Payments\YooKassa\PaymentMethod;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class YooKassaPaymentMethodCardResponseData extends Data
{
    public function __construct(
        public string $first6,
        public string $last4,
        public string $expiryMonth,
        public string $expiryYear,
        public string $cardType,
        public string $issuerCountry,
        public ?string $issuerName = null,
    ) {
    }
}
