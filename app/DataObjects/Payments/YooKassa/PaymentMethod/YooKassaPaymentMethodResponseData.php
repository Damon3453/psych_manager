<?php

declare(strict_types=1);

namespace App\DataObjects\Payments\YooKassa\PaymentMethod;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class YooKassaPaymentMethodResponseData extends Data
{
    public function __construct(
        public string $type,
        public string $id,
        public bool $saved,
        public string $title = '',
        public ?YooKassaPaymentMethodCardResponseData $card = null,
    ) {
    }
}
