<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Providers\Payments;

interface PaymentContract
{
    /**
     * Get a driver instance.
     *
     * @param string $driver
     *
     * @return Payments\Tinkoff|Payments\YooKassa
     */
    public function from(string $driver): Payments\AbstractProvider;
}
