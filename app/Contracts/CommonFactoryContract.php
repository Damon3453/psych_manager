<?php

declare(strict_types=1);

namespace App\Contracts;

interface CommonFactoryContract
{
    public function fromRequest(FormRequestContract $request): static;

    public function make(): ServiceModelContract;
}
