<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Dto\ClientDto;

interface ServiceModelContract
{
    public function loadFromDto(ClientDto $dto): static;
}
