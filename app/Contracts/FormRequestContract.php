<?php

declare(strict_types=1);

namespace App\Contracts;

interface FormRequestContract
{
    public function toDto(): CommonDtoContract;
}
