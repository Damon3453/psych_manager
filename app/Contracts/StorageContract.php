<?php

declare(strict_types=1);

namespace App\Contracts;

interface StorageContract
{
    public function setFactory(CommonFactoryContract $factory): static;
}
