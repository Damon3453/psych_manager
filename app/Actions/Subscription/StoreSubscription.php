<?php

declare(strict_types=1);

namespace App\Actions\Subscription;

use App\Models\Subscription;

class StoreSubscription
{
    /**
     * @param \App\DataObjects\Subscription\StoreSubscriptionData $data
     * @param \App\Models\User $user
     *
     * @return \App\Models\Subscription
     */
    public function handle(
        \Spatie\LaravelData\Contracts\DataObject $data,
        \Illuminate\Foundation\Auth\User $user
    ): \Illuminate\Database\Eloquent\Model {
        $subscription = Subscription::make([
            'payment_method_id' => $data->paymentMethodId,
            'status' => $data->status,
            'period' => $data->period,
            'cancel_date' => $data->cancelDate,
            'amount' => $data->amount,
            'response' => $data->response,
        ]);
        $subscription->user()->associate($user)->save();

        return $subscription;
    }
}
