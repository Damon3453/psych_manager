<?php

declare(strict_types=1);

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use App\Models\UserEmailVerify;

class VerifyMail extends Mailable implements ShouldQueue
{
    use Queueable;
    use SerializesModels;

    public function __construct(public UserEmailVerify $emailVerify)
    {
        $this->onQueue('emails');
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Verify',
        );
    }

    public function content(): Content
    {
        return new Content(
            markdown: 'emails.verify',
            with: [
                'url' => config('app.url') . '/email/confirm/' . $this->emailVerify->hash,
            ],
        );
    }

    public function attachments(): array
    {
        return [];
    }
}
