<?php

declare(strict_types=1);

if (!function_exists('resolveFilterDateRange')) {
    function resolveFilterDateRange(array $filterDates = []): array
    {
        $dateRange = [$filterDates[0], $filterDates[1]];
        if ($filterDates[0] === $filterDates[1]) {
            $dateRange[0] = \Carbon\Carbon::parse($filterDates[0])->startOfDay()->format('Y-m-d H:i');
            $dateRange[1] = \Carbon\Carbon::parse($filterDates[0])->endOfDay()->format('Y-m-d H:i');
        }

        return $dateRange;
    }
}
