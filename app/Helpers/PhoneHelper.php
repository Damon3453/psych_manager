<?php

declare(strict_types=1);

namespace App\Helpers;

class PhoneHelper
{
    public static function clear(?string $phone): ?string
    {
        if (is_null($phone)) {
            return null;
        }
        $phone = preg_replace('/\D/', '', trim($phone));

        return self::formatCountryCode($phone);
    }

    public static function formatCountryCode(?string $phone): ?string
    {
        if (is_null($phone)) {
            return null;
        }
        $strObject = str($phone);
        if ($strObject->startsWith('8')) {
            return $strObject->replace('8', '7')->value();
        }
        if ($strObject->startsWith('9') && $strObject->length() === 10) {
            return "7$phone";
        }

        return $phone;
    }
}
