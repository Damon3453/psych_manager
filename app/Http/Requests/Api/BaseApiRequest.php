<?php

declare(strict_types=1);

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

abstract class BaseApiRequest extends FormRequest
{
    /** @var array|string[] $email */
    protected array $email = ['required_without:phone', 'nullable', 'email:rfc,dns', 'max:255'];

    /** @var array|string[] $phone */
    protected array $phone = ['required_without:email', 'nullable', 'string', 'max:18', 'regex:^(?:\+7|8)\d{10}$'];

    /** @var array|string[] $connectionTypeLink */
    protected array $connectionTypeLink = ['required', 'string', 'nullable', 'max:255'];

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     *
     * @return void
     */
    protected function failedValidation(Validator $validator): void
    {
        $errors = (new ValidationException($validator))->errors();
        $message = (method_exists($this, 'message'))
            ? $this->container->call([$this, 'message'])
            : 'The given data was invalid.';
        if ($validator->fails()) {
            $message = $validator->errors()->first();
        }

        foreach ($errors as &$error) {
            $error = $error[0];
        }

        throw new HttpResponseException(new JsonResponse([
            'message' => $message,
            'errors' => $errors,
            'data' => (object) [],
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY, [], JSON_UNESCAPED_UNICODE));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array>
     */
    abstract public function rules(): array;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !is_null($this->user());
    }

    // protected function sanitize(): void
    // {
    //     $inputs = $this->all();
    //     $inputs['phone'] = PhoneHelper::clear($inputs['phone']);
    //     $this->replace($inputs);
    // }

    /**
     * @PhpCsFixer::ignore{"final_public_method_for_abstract_class"}
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'phone.unique' => __('This phone already exists'),
            'email.unique' => __('This email already exists'),
            'connection_type_id.required' => __('Please, select connection type'),
        ];
    }
}
