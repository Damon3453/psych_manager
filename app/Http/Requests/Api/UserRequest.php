<?php

declare(strict_types=1);

namespace App\Http\Requests\Api;

use App\Helpers\PhoneHelper;

class UserRequest extends BaseApiRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'phone' => PhoneHelper::clear($this->input('phone')),
        ]);
    }

    /** @inheritdoc */
    public function rules(): array
    {
        // $this->sanitize();
        $this->phone = ['required_without:email', 'string', 'max:18', 'unique:users,phone,' . $this->user()->id];
        $this->email = [
            'required_without:phone',
            'nullable',
            'email:rfc,dns',
            'max:255',
            'unique:users,email,' . $this->user()->id,
        ];
        if ($this->user()->phone !== $this->input('phone')) {
            $this->phone = ['required', 'string', 'max:18', 'unique:users,phone,' . $this->user()->id];
        }

        return [
            'name' => ['required', 'string', 'max:255'],
            'phone' => $this->phone,
            'email' => $this->email,
            'gender' => ['sometimes', 'nullable', 'boolean'],
            'site' => ['nullable', 'string', 'max:255'],
        ];
    }
}
