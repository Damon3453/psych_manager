<?php

declare(strict_types=1);

namespace App\Http\Requests\Api;

use OpenApi\Annotations as OA;
use App\Helpers\PhoneHelper;
use App\Contracts\FormRequestContract;
use App\Dto\ClientDto;
use App\Dto\TherapyDto;

/**
 * Class ClientRequest
 *
 * @OA\Schema(schema="ClientRequest",
 *     @OA\Property(property="name", type="string", description="Имя"),
 *     @OA\Property(property="email", type="string", example="someuser@gmail.com", description="Email"),
 *     @OA\Property(property="phone", type="string", example="88005553535", description="Телефон"),
 *     @OA\Property(property="role", type="integer", description="Роль: клиент или специалист"),
 *     @OA\Property(property="gender", type="integer", description="Пол"),
 *     @OA\Property(property="connection_type", type="integer", example=1, description="Id предпочитаемого способа связи"),
 * )
 */
class ClientRequest extends BaseApiRequest implements FormRequestContract
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'phone' => PhoneHelper::clear($this->input('phone')),
        ]);
    }

    /**
     * @return array<string, array>
     */
    public function rules(): array
    {
        // $this->sanitize();
        if ($this->isMethod('POST')) {
            $this->email[] = 'unique:clients';
            $this->email[] = 'unique:users';
            $this->phone[] = 'unique:clients';
        }

        return [
            'name' => ['sometimes', 'nullable', 'string', 'max:255'],
            'email' => $this->email,
            'phone' => $this->phone,
            'category_id' => ['sometimes', 'integer', 'exists:categories,id'],
            'birthday_date' => ['sometimes', !$this->isEmptyString('birthday_date') ? 'date_format:"Y-m-d"' : ''],
            'gender' => ['sometimes', 'nullable', 'boolean'],
            'connection_type_id' => ['required', 'integer'],
            // add regular expression
            'connection_type_link' => $this->connectionTypeLink,
            'curator_contacts' => ['sometimes', 'string', 'nullable', 'max:255'],
            'meeting_type' => ['sometimes', 'nullable', 'boolean'],
            'therapy.problem_severity' => ['sometimes', 'nullable', 'integer'],
            'therapy.plan' => ['sometimes', 'nullable', 'string'],
            'therapy.request' => ['sometimes', 'nullable', 'string'],
            'therapy.notes' => ['sometimes', 'nullable', 'string'],
            'therapy.concept_vision' => ['sometimes', 'nullable', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages(): array
    {
        return [
            'category_id.required' => 'Поле Категория обязательно для заполнения',
            'category_id.integer' => 'Поле Категория обязательно для заполнения',
            'connection_type_id.required' => 'Поле Способ связи обязательно для заполнения',
        ];
    }

    public function toDto(): ClientDto
    {
        $dto = ClientDto::make($this->except('therapy'));
        $dto->id = $this->route('client');
        $dto->user_id = $this->user()->id;

        $dto->therapy = TherapyDto::make($this->input('therapy'));

        return $dto;
    }
}
