<?php

declare(strict_types=1);

namespace App\Http\Requests\Api;

use Illuminate\Validation\Rule;
use OpenApi\Annotations as OA;
use App\Rules\SessionDateDisableNightTime;

/**
 * Class SessionRequest
 *
 * @OA\Schema(schema="SessionRequest",
 *     @OA\Property(property="client_id", type="integer"),
 *     @OA\Property(property="user_id", type="integer"),
 *     @OA\Property(property="session_date", type="string", description="date_format Y-m-d H:i, after_or_equal:today"),
 *     @OA\Property(property="comment", type="string", description="План на сессию"),
 * )
 */
class SessionRequest extends BaseApiRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $sessionDateCopy = [];
        $isCloneRoute = $this->routeIs('sessions.clone');
        if ($isCloneRoute) {
            $sessionDateCopy = ['required', 'date_format:Y-m-d', 'after_or_equal:today'];
        }

        return [
            'client_id' => ['required', 'int', 'max:255'],
            'user_id' => ['sometimes', 'int', 'max:255'],
            'session_date' => [
                'sometimes',
                Rule::requiredIf(!$isCloneRoute),
                'date_format:Y-m-d',
                'after_or_equal:today',
            ],
            'session_time_start' => [
                'sometimes',
                Rule::requiredIf(!$isCloneRoute),
                'date_format:H:i',
                new SessionDateDisableNightTime(),
            ],
            'session_time_end' => [
                'sometimes',
                Rule::requiredIf(!$isCloneRoute),
                'date_format:H:i',
                'after_or_equal:session_time_start',
            ],
            'comment' => ['sometimes', 'nullable', 'string', 'max:65534'],
            'session_date_copy' => $sessionDateCopy,
        ];
    }

    /**
     * @inheritdoc
     */
    public function messages(): array
    {
        return [
            'client_id.required' => 'Поле Клиент обязательно для заполнения',
            'session_date.after_or_equal' => 'Поле Дата сессии не должно быть ранее текущей даты',
        ];
    }
}
