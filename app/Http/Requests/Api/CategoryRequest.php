<?php

declare(strict_types=1);

namespace App\Http\Requests\Api;

use OpenApi\Annotations as OA;
use App\Contracts\{FormRequestContract, CommonDtoContract};
use App\Dto\CategoryRequestDto;

/**
 * Class CategoryRequest
 *
 * @OA\Schema(schema="CategoryRequest",
 *     @OA\Property(property="name", type="string", description="Category name")
 * )
 */
class CategoryRequest extends BaseApiRequest implements FormRequestContract
{
    #[\JetBrains\PhpStorm\ArrayShape([
        'categories' => 'array', 'categories.*' => 'string',
    ])]
    public function rules(): array
    {
        return [
            // 'categories' => ['required', 'array'],
            // 'categories.*' => ['required_with:categories', 'string', 'max:255'],
            'category' => ['required', 'string', 'max:255'],
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCategories(): \Illuminate\Support\Collection
    {
        return $this->collect('categories')->pluck('id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getNewCategories(): \Illuminate\Support\Collection
    {
        return $this->collect('newCategories')->unique();
    }

    public function toDto(): CommonDtoContract
    {
        $dto = CategoryRequestDto::make();
        $dto->categories = $this->getCategories();
        $dto->newCategories = $this->getNewCategories();

        return $dto;
    }
}
