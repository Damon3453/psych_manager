<?php

declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Http\Requests\Api\BaseApiRequest;
use App\DataValueObjects\PhoneValue;
use App\Models\UserPhoneVerify;

class SendSmsRequest extends BaseApiRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'phone' => PhoneValue::make($this->input('phone'))->toNative(),
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'type' => [
                'required',
                'int',
                'in:' . UserPhoneVerify::TYPE_REGISTER . ',' . UserPhoneVerify::TYPE_LOGIN,
                // 'min:' . UserPhoneVerify::TYPE_REGISTER,
                // 'max:' . UserPhoneVerify::TYPE_LOGIN,
            ],
            'phone' => [
                'required',
                'string',
                'max:255',
                $this->isLoginAction() ? 'exists:users' : 'unique:users',
            ],
        ];
    }

    public function isLoginAction(): bool
    {
        return $this->input('type') === UserPhoneVerify::TYPE_LOGIN;
    }
}
