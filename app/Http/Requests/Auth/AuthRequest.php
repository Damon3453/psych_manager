<?php

declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Http\Requests\Api\BaseApiRequest;
use App\DataValueObjects\PhoneValue;

class AuthRequest extends BaseApiRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'phone' => PhoneValue::make($this->input('phone'))->toNative(),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array|\Illuminate\Contracts\Validation\ValidationRule|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'nullable', 'string', 'max:255'],
            'phone' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
