<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     title="Psych Manager Origin API",
 *     version="1.0 beta",
 *     @OA\Contact(email="info@ps.ru")
 * )
 * @OA\Server(
 *     description="PMO server",
 *     url="http://localhost/api/v1"
 * )
 * @OA\SecurityScheme(
 *     type="oauth2",
 *     in="header",
 *     name="Bearer",
 *     securityScheme="passport"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
}
