<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Passport\Http\Controllers\AccessTokenController as ATC;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Auth\SendSmsRequest;
use App\Jobs\SendEmailVerification;
use App\Jobs\SendSms;
use App\Notifications\DiscordNotification;
use App\Services\App\Client;
use App\Services\UserService;
use App\DataValueObjects\PhoneValue;
use App\Actions\Subscription\StoreSubscription;
use App\DataObjects\Subscription\StoreSubscriptionData;
use App\Models\{User, UserEmailVerify, UserPhoneVerify};

class VerificationController extends ATC
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected string $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @param \League\OAuth2\Server\AuthorizationServer $server
     * @param \Laravel\Passport\TokenRepository $tokens
     * @param ResponseContract $json
     */
    public function __construct(
        AuthorizationServer $server,
        TokenRepository $tokens,
        public ResponseContract $json
    ) {
        parent::__construct($server, $tokens);
    }

    public function verifyEmail(Request $request)
    {
        $user = UserEmailVerify::where('hash', $request->input('hash'))->firstOrFail()->user;
        if (
            !hash_equals(
                (string) $request->input('hash'),
                sha1($user->getEmailForVerification() . $user->getAuthIdentifier())
            )
        ) {
            throw new AuthorizationException();
        }
        if ($user->hasVerifiedEmail()) {
            return $this->json->response([], 'Email уже подтверждён', JsonResponse::HTTP_NO_CONTENT);
        }
        $user->markEmailAsVerified();

        return $this->json->response([], 'Успешно', JsonResponse::HTTP_NO_CONTENT);
    }

    public function resend(Request $request): JsonResponse
    {
        $request->validate([
            'email' => ['required', 'string', 'email:rfc,dns', 'max:255', 'exists:users,email'],
        ]);
        $email = str($request->input('email'))->trim()->lower()->value();
        $user = User::firstWhere('email', $email);

        try {
            \DB::beginTransaction();

            if ($user->hasVerifiedEmail()) {
                return $this->json->response([], 'Email уже подтверждён');
            }
            $user->userEmailVerify()->delete();
            $userEmailVerify = new UserEmailVerify(['email' => $email]);
            $user->userEmailVerify()->save($userEmailVerify);
            SendEmailVerification::dispatch($user)->afterCommit();

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();
            app(DiscordNotification::class)->setExceptionsEmbedFields($e)->toDiscord();

            return $this->json->error($e->getMessage(), $e->getCode());
        }

        return $this->json->response([], 'Письмо со ссылкой подтверждения отправлено на указанный email');
    }

    /**
     * @deprecated
     *
     * @param SendSmsRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSms(SendSmsRequest $request): \Illuminate\Http\JsonResponse
    {
        $phone = PhoneValue::make($request->input('phone'));
        if ($request->isLoginAction()) {
            $user = User::firstWhere('phone', $phone->toNative());
            $userPhoneVerify = UserPhoneVerify::make([
                'phone' => $phone->toNative(),
                'type' => $request->input('type'),
            ]);
            $userPhoneVerify->user()->associate($user)->save();
        } else {
            $userPhoneVerify = UserPhoneVerify::create([
                'phone' => $phone->toNative(),
                'type' => $request->input('type'),
            ]);
        }
        SendSms::dispatch(
            $phone,
            "Код подтверждения: $userPhoneVerify->code",
        );

        return $this->json->response([], 'Успешно');
    }

    /**
     * @deprecated
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\UserService $service
     * @param \App\Actions\Subscription\StoreSubscription $action
     *
     * @throws \JsonException
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPhone(
        Request $request,
        UserService $service,
        StoreSubscription $action
    ): \Illuminate\Http\JsonResponse {
        $client = new Client(
            app(\Psr\Http\Message\ServerRequestInterface::class)
        );
        $psr7Request = $client->parseRequestBody();

        $request->validate([
            'phone' => ['required', 'string', 'max:255'],
            'code' => ['required', 'string', 'min:6', 'max:6'],
        ]);
        $phone = PhoneValue::make($request->input('phone'));
        $user = User::query()->where('phone', $phone->toNative())->firstOrFail();

        $service->setUser($user)
            ->setInput($request->collect())
            ->verifyPhone(UserPhoneVerify::TYPE_REGISTER);

        if (!$user->subscription) {
            $data = StoreSubscriptionData::from([
                'paymentMethodId' => null,
                'status' => 'succeeded',
                'period' => 0,
                'cancelDate' => \Carbon\Carbon::now()->addWeeks(2),
                'amount' => '0',
                'response' => null,
            ]);
            $action->handle($data, $user);
        }

        // generate token
        $tokenResponse = parent::issueToken($psr7Request);
        // convert response to json string
        $content = $tokenResponse->getContent();
        // convert json to array
        $data = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        return $this->json->response($data, 'Пробный период активирован');
    }
}
