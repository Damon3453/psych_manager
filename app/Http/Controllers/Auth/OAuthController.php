<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Http\Controllers\AccessTokenController as ATC;
use App\Http\Requests\Auth\AuthRequest;
use App\Jobs\SendSms;
use App\Actions\Subscription\StoreSubscription;
use App\Notifications\DiscordNotification;
use App\Exceptions\EntityStoreException;
use App\Exceptions\TypeErrorHttpException;
use App\Services\UserService;
use App\Services\App\Client;
use App\DataObjects\Subscription\StoreSubscriptionData;
use App\DataObjects\User\UserRegisterData;
use App\DataValueObjects\PhoneValue;
use App\Models\{User, UserPhoneVerify};

class OAuthController extends ATC
{
    /**
     * Create a new controller instance.
     *
     * @param AuthorizationServer $server
     * @param TokenRepository $tokens
     * @param ResponseContract $json
     */
    #[Pure]
    public function __construct(
        AuthorizationServer $server,
        TokenRepository $tokens,
        private readonly ResponseContract $json,
    ) {
        parent::__construct($server, $tokens);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @throws \Throwable
     *
     * @return User
     */
    protected function create(array $data): User
    {
        $userData = UserRegisterData::from($data);
        $user = User::query()->firstOrNew([
            'phone' => $userData->phone,
        ], [
            'name' => $userData->name,
        ]);

        DB::beginTransaction();

        if (!$user->save()) {
            DB::rollBack();
            throw new EntityStoreException('User not saved');
        }
        $userPhoneVerify = new UserPhoneVerify(['phone' => $user->phone]);
        if (!$user->userPhoneVerifies()->save($userPhoneVerify)) {
            DB::rollBack();
            throw new EntityStoreException('User phone verify not saved');
        }
        SendSms::dispatch(
            $user->phone,
            'Код подтверждения: ' . $user->userPhoneVerifies()->latest('id')->value('code')
        );

        DB::commit();

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param AuthRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(AuthRequest $request): JsonResponse
    {
        try {
            $this->create($request->validated());
        } catch (\Throwable $exception) {
            $e = new TypeErrorHttpException(
                $exception->getMessage(),
                \Illuminate\Http\JsonResponse::HTTP_UNSUPPORTED_MEDIA_TYPE
            );
            app(DiscordNotification::class)->setExceptionsEmbedFields($e)->toDiscord();

            return $this->json->error($e->getMessage(), $e->getStatusCode());
        }

        return $this->json->stored([]);
    }

    /**
     * Authorize a client to access the user's account.
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse|Response
     */
    public function issueToken(ServerRequestInterface $request): JsonResponse|Response
    {
        if (\Laravel\Passport\Client::query()->doesntExist()) {
            return $this->json->OAuthError(
                'Wrong Client data.',
                'invalid_config',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $service = app(UserService::class);
        $action = app(StoreSubscription::class);
        $client = new Client($request);

        try {
            $request = $client->parseRequestBody();

            // get grant type
            // $grantType = $client->getGrantType();

            // if ($grantType === 'password') {
            //     // get username (default is :email)
            //     $email = $client->getUsername();
            //
            //     if (!User::query()->firstWhere('email', $email)) {
            //         return $this->json->OAuthError(
            //             'The user credentials were incorrect.',
            //             'invalid_credentials',
            //             Response::HTTP_UNAUTHORIZED
            //         );
            //     }
            // }

            // if ($grantType === 'phone') {
            $phone = PhoneValue::make($client->getPhone());
            $user = User::query()->where('phone', $phone->toNative())->firstOrFail();

            if (!$user) {
                return $this->json->OAuthError(
                    'The user credentials were incorrect.',
                    'invalid_credentials',
                    Response::HTTP_UNAUTHORIZED
                );
            }
            if (!$user->subscription) {
                $data = StoreSubscriptionData::from([
                    'paymentMethodId' => null,
                    'status' => 'succeeded',
                    'period' => 0,
                    'cancelDate' => \Carbon\Carbon::now()->addWeeks(2),
                    'amount' => '0',
                    'response' => null,
                ]);
                $action->handle($data, $user);
            }

            // generate token
            $tokenResponse = parent::issueToken($request);
            // convert response to json string
            $content = $tokenResponse->getContent();
            // convert json to array
            $data = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

            if (isset($data['error'])) {
                return $this->json->OAuthError(
                    'The user credentials were incorrect.',
                    'invalid_credentials',
                    Response::HTTP_UNAUTHORIZED
                );
            }

            $service
                ->setUser($user)
                ->setInput(collect($request->getParsedBody()))
                ->verifyPhone();

            return response()->json($data, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return $this->json->OAuthError(
                $exception->getMessage(),
                OAuthServerException::invalidGrant()->getErrorType(),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Logout user and invalidate token.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()?->revoke();

        return response()->json(status: Response::HTTP_NO_CONTENT);
    }
}
