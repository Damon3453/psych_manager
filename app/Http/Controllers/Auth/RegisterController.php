<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use App\Exceptions\{EntityStoreException, TypeErrorHttpException};
use App\Notifications\DiscordNotification;
use App\Jobs\SendSms;
use App\DataObjects\User\UserRegisterData;
use App\Models\{User, UserPhoneVerify};

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default, this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration
     *
     * @var string
     */
    protected string $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance
     *
     * @param ResponseContract $json
     *
     * @return void
     */
    public function __construct(public ResponseContract $json)
    {
        $this->middleware('guest');
    }

    /**
     * @deprecated
     *
     * Create a new user instance after a valid registration
     *
     * @param array $data
     *
     * @throws \Throwable
     *
     * @return User
     */
    protected function create(array $data): User
    {
        $user = User::make(UserRegisterData::from($data)->toArray());

        DB::beginTransaction();

        if (!$user->save()) {
            DB::rollBack();
            throw new EntityStoreException('User not saved');
        }
        $userPhoneVerify = new UserPhoneVerify(['phone' => $user->phone]);
        if (!$user->userPhoneVerifies()->save($userPhoneVerify)) {
            DB::rollBack();
            throw new EntityStoreException('User phone verify not saved');
        }
        SendSms::dispatch(
            $user->phone,
            'Код подтверждения: ' . $user->userPhoneVerifies()->latest('id')->value('code')
        );

        DB::commit();

        return $user;
    }

    /**
     * @deprecated
     *
     * Handle a registration request for the application
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        try {
            $user = $this->create($request->all());
        } catch (\Throwable $exception) {
            $e = new TypeErrorHttpException(
                $exception->getMessage(),
                \Illuminate\Http\JsonResponse::HTTP_UNSUPPORTED_MEDIA_TYPE
            );
            app(DiscordNotification::class)->setExceptionsEmbedFields($e)->toDiscord();

            return $this->json->error($e->getMessage(), $e->getStatusCode());
        }

        $this->guard()->login($user);

        return $this->json->stored([]);
    }
}
