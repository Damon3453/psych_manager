<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Payments;

use App\Actions\Subscription\StoreSubscription;
use App\DataObjects\Subscription\StoreSubscriptionData;
use Illuminate\Http\Request;
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use App\Providers\Payments\YooKassa;
use App\DataObjects\Payments\YooKassa\Notification\YooKassaPaymentStatusData;
use App\Contracts\PaymentContract;
use App\Models\{PaymentProvider, Subscription, PaymentNotification};

class YooKassaController
{
    public YooKassa $service;

    /**
     * @param \Pepperfm\ApiBaseResponder\Contracts\ResponseContract $json
     * @param \App\Services\Payments\PaymentManager $factory
     */
    public function __construct(public ResponseContract $json, public PaymentContract $factory)
    {
        $provider = PaymentProvider::query()->firstWhere('name', 'YooKassa');
        $this->service = $factory->from($provider->name);
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     *
     * @param Request $request
     * @param string $paymentId
     */
    public function getPaymentInfo(Request $request, string $paymentId)
    {
        // $request->validate([
        //     'payment_id' => ['required', 'string', 'max:36'],
        // ]);
        return $this->service->getPaymentInfo($paymentId);
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     */
    public function createPayment()
    {
        return $this->service->createPayment();
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     *
     * @param StoreSubscription $action
     */
    public function createSubscription(StoreSubscription $action)
    {
        $response = $this->service->charge();
        $data = StoreSubscriptionData::from([
            'paymentMethodId' => $response->paymentMethod->id,
            'status' => $response->status,
            'period' => 1,
            'cancelDate' => \Carbon\Carbon::now()->addMonths(),
            'amount' => '1000',
            'response' => $response,
        ]);
        $action->handle($data, \Auth::user());

        return $response;
    }

    /**
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     */
    public function handleSubscription()
    {
        return $this->service->handleSubscription();
    }

    /**
     * @param Request $request
     *
     * @throws \Throwable
     */
    public function handlePaymentEvent(Request $request): void
    {
        ray($request->all())->blue();

        $yooKassaNotification = YooKassaPaymentStatusData::from($request->input('object'));
        $paymentNotification = PaymentNotification::make([
            'id' => $yooKassaNotification->id,
            'event' => $request->input('event'),
            'status' => $yooKassaNotification->status,
            'amount_paid' => $yooKassaNotification->amount['value'],
            'amount_income' => $yooKassaNotification->incomeAmount['value'] ?? null,
            'recipient_account_id' => $yooKassaNotification->recipient['account_id'],
            'recipient_gateway_id' => $yooKassaNotification->recipient['gateway_id'],
            'payment_method' => $yooKassaNotification->paymentMethod,
            'captured_at' => $yooKassaNotification->capturedAt,
            'paid' => $yooKassaNotification->paid,
            'refundable' => $yooKassaNotification->refundable,
            'authorization_details' => $yooKassaNotification->authorizationDetails,
            'cancellation_details' => $yooKassaNotification->cancellationDetails,
            'created_at' => $yooKassaNotification->createdAt,
        ]);
        $subscription = Subscription::query()->firstWhere(
            'payment_method_id',
            $yooKassaNotification->paymentMethod->id
        );
        $paymentNotification->payment_method_id = $subscription?->payment_method_id;
        $paymentNotification->save();

        if ($subscription) {
            try {
                \DB::beginTransaction();

                $previousResponse = $subscription->response;
                $previousResponse->status = $paymentNotification->status;
                $previousResponse->paid = $paymentNotification->paid;
                $previousResponse->cancellation_details = $paymentNotification->cancellation_details;
                $subscription->response = $previousResponse;
                $subscription->status = $paymentNotification->status;
                $subscription->save();

                \DB::commit();
            } catch (\Throwable $e) {
                \DB::rollBack();

                ray($e->getMessage())->red();
            }
        }

        // return $this->service->payOfSubscription();
    }
}
