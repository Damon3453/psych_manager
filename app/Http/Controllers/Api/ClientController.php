<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\{Auth, DB, Log};
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;
use App\Http\Requests\Api\ClientRequest;
use App\Http\Resources\Api\Client\{IndexResource, ShowResource};
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use App\Services\ClientService;
use App\Models\Client;

class ClientController
{
    public function __construct(
        protected ResponseContract $json,
        protected ClientService $clientService
    ) {
    }

    /**
     * @OA\Get(
     *     path="/clients",
     *     operationId="clients-index",
     *     tags={"Клиенты"},
     *     summary="Index page",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\Response(
     *         response="200",
     *         description="",
     *         @OA\JsonContent(ref="#/components/schemas/ClientIndexResource")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Список пуст",
     *     ),
     * )
     *
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $clients = $this->clientService->getUsersWithFilters($request->input('options', []), $total, Auth::user());

        return $this->json->response(['clients' => IndexResource::collection($clients), 'total' => $total]);
    }

    /**
     * @OA\Post (
     *     path="/clients",
     *     operationId="clients-store",
     *     tags={"Клиенты"},
     *     summary="Store",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\RequestBody(
     *         required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/ClientRequest"),
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Клиент добавлен",
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка создания",
     *     ),
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param ClientRequest $request
     *
     * @throws \Throwable
     *
     * @return JsonResponse
     */
    public function store(ClientRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $client = $this->clientService->getFactory()->fromRequest($request)->make();
            $this->clientService->saveClient($client);

            DB::commit();
        } catch (\App\Exceptions\EntityException $e) {
            DB::rollBack();

            return $this->json->response([], $e->getMessage(), $e->getStatusCode());
        }

        return $this->json->response([], 'Клиент добавлен', JsonResponse::HTTP_CREATED);
    }

    /**
     * @OA\Get(
     *     path="/clients/{id}",
     *     operationId="clients-show",
     *     tags={"Клиенты"},
     *     summary="Show page",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="id пользователя",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="",
     *         @OA\JsonContent(ref="#/components/schemas/ClientShowResource")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Клиент не найден",
     *     ),
     * )
     *
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $client = Client::query()->withTrashed()
            ->findOrFail($id)
            ->load([
                'sessions' => static fn(HasMany $q) => $q->orderBy('session_date'),
                'connectionType', 'category', 'therapy', 'latestSession',
            ]);

        return $this->json->response(['client' => ShowResource::make($client)]);
    }

    /**
     * @OA\Put(
     *     path="/clients/{id}",
     *     operationId="clients-update",
     *     tags={"Клиенты"},
     *     summary="Update",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *              type="object",
     *              allOf={
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="id", type="integer", example=5),
     *                  ),
     *                  @OA\Schema(ref="#/components/schemas/ClientRequest"),
     *              }
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Клиент обновлён",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Клиент не найден",
     *     ),
     * )
     *
     * Update the specified resource in storage.
     *
     * @param ClientRequest $request
     *
     * @throws \Throwable
     *
     * @return JsonResponse
     */
    public function update(ClientRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $client = $this->clientService->getFactory()->fromRequest($request)->make();
            $this->clientService->saveClient($client);

            DB::commit();
        } catch (\App\Exceptions\EntityException $e) {
            DB::rollBack();
            Log::critical($e);

            return $this->json->error(
                'Ошибка обновления клиента',
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                $e->getMessage()
            );
        }

        return $this->json->response([], 'Клиент обновлён');
    }

    /**
     * @OA\Delete (
     *     path="/clients/{id}",
     *     operationId="clients-delete",
     *     tags={"Клиенты"},
     *     summary="Delete",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="id пользователя",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Клиент удалён",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Клиент не найден",
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка удаления",
     *     ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $client = Client::findOrFail($id);
        if (!$client->delete()) {
            return $this->json->error('Ошибка удаления');
        }

        return $this->json->response([], 'Завершено. Перенесён в конец списка');
    }

    /**
     * @return JsonResponse
     */
    public function getSessionClients(): JsonResponse
    {
        return $this->json->response(['clients' => Auth::user()?->clients()->select(['id', 'name'])->get()]);
    }

    public function restore($id): JsonResponse
    {
        $client = Client::withTrashed()->findOrFail($id);
        if (!$client->restore()) {
            return $this->json->error('Ошибка восстановления');
        }

        return $this->json->response([], 'Возобновлено');
    }
}
