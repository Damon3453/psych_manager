<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use App\Http\Resources\Api\User\UserResource;
use App\Http\Resources\Api\CategoryResource;
use App\Http\Requests\Api\{CategoryRequest, UserRequest};
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use App\Services\UserService;

class UserController
{
    public function __construct(
        protected ResponseContract $json,
        protected UserService $userService
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $user = \Auth::user()?->load(['categories']);

        return $this->json->response(['user' => UserResource::make($user)]);
    }

    /**
     * Sync categories
     *
     * @param CategoryRequest $request
     *
     * @return JsonResponse
     */
    public function syncCategories(CategoryRequest $request): JsonResponse
    {
        // $coreModel = $this->userService->getFactory()->fromRequest($request)->make();

        try {
            \DB::beginTransaction();

            $category = $this->userService
                ->setUser($request->user())
                ->setCategory($request->input('category'))
                ->saveCategory();

            \DB::commit();
        } catch (\App\Exceptions\EntityStoreException $e) {
            \DB::rollBack();

            return $this->json->error($e->getMessage());
        }

        return $this->json->response([
            'category' => CategoryResource::make($category),
        ], 'Сохранено', JsonResponse::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     *
     * @return JsonResponse
     */
    public function update(UserRequest $request): JsonResponse
    {
        $user = $request->user();
        if (!$user->update($request->validated())) {
            return $this->json->error(httpStatusCode: JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->json->response([
            'user' => UserResource::make($user),
        ], 'Данные обновлены');
    }
}
