<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use Illuminate\Http\{JsonResponse, Request};
use OpenApi\Annotations as OA;
use Pepperfm\ApiBaseResponder\Contracts\ResponseContract;
use App\Http\Requests\Api\SessionRequest;
use App\Http\Resources\Api\Sessions\{IndexResource, ShowResource, CalendarResource};
use App\Services\SessionService;
use App\Models\Session;

class SessionController
{
    public function __construct(
        protected ResponseContract $json,
        public SessionService $sessionService
    ) {
    }

    /**
     * @OA\Get(
     *     path="/sessions",
     *     operationId="sessions-index",
     *     tags={"Сессии"},
     *     summary="Index page",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\Response(
     *         response="200",
     *         description="",
     *         @OA\JsonContent(ref="#/components/schemas/SessionIndexResource"),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Список пуст",
     *     ),
     * )
     *
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @throws \Throwable
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $sessions = $this->sessionService->getSessionsWithFilters(
            $request->input('options', []),
            $total,
            \Auth::user()
        );

        return $this->json->response(['sessions' => IndexResource::collection($sessions), 'total' => $total]);
    }

    /**
     * @OA\Post (
     *     path="/sessions",
     *     operationId="sessions-store",
     *     tags={"Сессии"},
     *     summary="Store",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\RequestBody(
     *         required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SessionRequest"),
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Клиент добавлен",
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка создания",
     *     ),
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param SessionRequest $request
     *
     * @throws \Throwable
     *
     * @return JsonResponse
     */
    public function store(SessionRequest $request): JsonResponse
    {
        try {
            \DB::beginTransaction();

            $this->sessionService->setData($request->collect())->updateOrCreate($request->user());

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();
            \Log::error($e->getMessage(), [
                'trace' => $e->getTraceAsString(),
            ]);

            return $this->json->error($e->getMessage());
        }

        return $this->json->stored([], 'Запись создана');
    }

    /**
     * @OA\Get(
     *     path="/sessions/{id}",
     *     operationId="sessions-show",
     *     tags={"Сессии"},
     *     summary="Show page",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="id сессии",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="",
     *         @OA\JsonContent(ref="#/components/schemas/SessionShowResource")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Запись не найдена",
     *     ),
     * )
     *
     * Display the specified resource.
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $session = Session::q()->findOrFail($id)->load('client');

        return $this->json->response(['session' => ShowResource::make($session)]);
    }

    /**
     * @OA\Put(
     *     path="/sessions/{id}",
     *     operationId="sessions-update",
     *     tags={"Сессии"},
     *     summary="Update",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *              type="object",
     *              allOf={
     *                  @OA\Schema(
     *                      type="object",
     *                      @OA\Property(property="id", type="integer", example=5),
     *                  ),
     *                  @OA\Schema(ref="#/components/schemas/SessionRequest"),
     *              }
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Клиент обновлён",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Клиент не найден",
     *     ),
     * )
     *
     * Update the specified resource in storage.
     *
     * @param $id
     * @param SessionRequest $request
     *
     * @throws \Throwable
     *
     * @return JsonResponse
     */
    public function update($id, SessionRequest $request): JsonResponse
    {
        try {
            \DB::beginTransaction();

            $this->sessionService->setData($request->collect())->updateOrCreate(\Auth::user(), $id);

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();
            \Log::error($e->getMessage(), [
                'trace' => $e->getTraceAsString(),
            ]);

            return $this->json->error($e->getMessage(), JsonResponse::HTTP_NOT_FOUND);
        }

        return $this->json->response([], 'Запись обновлена');
    }

    /**
     * @OA\Delete (
     *     path="/sessions/{id}",
     *     operationId="sessions-delete",
     *     tags={"Сессии"},
     *     summary="Delete",
     *     security={
     *         {"default": {}}
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="id сессии",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="cancel_reason", type="string", description="Причина отмены"),
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Запись отменена",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Запись не найдена",
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка отмены",
     *     ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $session = Session::q()->findOrFail($id);
        if (!$session->delete()) {
            return $this->json->error('Ошибка удаления');
        }

        return $this->json->response([], 'Запись отменена');
    }

    /**
     * @throws \Throwable
     *
     * @return JsonResponse
     */
    public function getCalendarSessions(): JsonResponse
    {
        $sessions = $this->sessionService->getLastMonthSessions();

        return $this->json->response(['sessions' => CalendarResource::collection($sessions)]);
    }

    /**
     * @param $id
     * @param \App\Http\Requests\Api\SessionRequest $request
     *
     * @return void
     */
    public function clone($id, SessionRequest $request): void
    {
        $session = Session::findOrFail($id);
        $session->fill($request->validated())->setSessionDate($request->input('session_date_copy'));
        $session->duplicate();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string|null $date
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeSessionFromToTimeRange(\Illuminate\Http\Request $request, string $date = null): JsonResponse
    {
        return $this->json->response([
            'time' => $this->sessionService->setUser(\Auth::user())->getTodaySessions($date ?? $request->query('date')),
        ]);
    }

    public function pushSessionToCalendar(int $id): void
    {
        $session = Session::findOrFail($id);
        try {
            // $events = \Spatie\GoogleCalendar\Event::get();
            \Spatie\GoogleCalendar\Event::create([
                'name' => 'A new event',
                'startDateTime' => \Carbon\Carbon::now(),
                'endDateTime' => \Carbon\Carbon::now()->addHour(),
            ]);
        } catch (\Exception $e) {
            dd($e, $session);
        }
    }
}
