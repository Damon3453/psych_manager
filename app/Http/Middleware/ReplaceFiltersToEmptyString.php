<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class ReplaceFiltersToEmptyString extends TransformsRequest
{
    /**
     * Transform the given value.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return mixed
     */
    protected function transform($key, $value): mixed
    {
        if (\Str::startsWith($key, 'field')) {
            return $value ?? '';
        }

        return $value;
    }
}
