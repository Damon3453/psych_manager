<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Subscribed
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = $request->user();
        if (!$user) {
            return \ApiBaseResponder::response([], 'Token problem', JsonResponse::HTTP_NOT_FOUND);
        }
        if ($user->email === config('app.admin_email')) {
            return $next($request);
        }
        if (!$user->subscription) {
            return \ApiBaseResponder::response([], 'Подписка истекла', JsonResponse::HTTP_FORBIDDEN);
        }
        if (
            $user->subscription->cancel_date->lt(\Carbon\Carbon::today()) ||
            $user->subscription->status !== 'succeeded'
        ) {
            return \ApiBaseResponder::response([], 'Подписка истекла', JsonResponse::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
