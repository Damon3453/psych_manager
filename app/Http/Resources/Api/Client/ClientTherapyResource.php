<?php

declare(strict_types=1);

namespace App\Http\Resources\Api\Client;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/** @mixin \App\Models\ClientTherapy */
class ClientTherapyResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    #[\JetBrains\PhpStorm\ArrayShape([
        'id' => 'int', 'request' => 'null|string',
        'problem_severity' => 'int|null', 'plan' => 'null|string',
        'notes' => 'null|string', 'concept_vision' => 'null|string',
        'client_id' => 'int', 'client' => IndexResource::class | null,
    ])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'request' => $this->request,
            'problem_severity' => $this->problem_severity,
            'plan' => $this->plan,
            'notes' => $this->notes,
            'concept_vision' => $this->concept_vision,
            'client_id' => $this->client_id,
            'client' => IndexResource::make($this->whenLoaded('client')),
        ];
    }
}
