<?php

declare(strict_types=1);

namespace App\Http\Resources\Api\Sessions;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;
use App\Http\Resources\Api\Client\ShowResource;

/**
 * Class IndexResource
 *
 * @mixin \App\Models\Session
 *
 * @OA\Schema(schema="SessionIndexResource",
 *     type="object",
 *     @OA\Property(property="id", type="string"),
 *     @OA\Property(property="name", type="string"),
 *     @OA\Property(property="email", type="string"),
 *     @OA\Property(property="connection_type", type="integer", description="id записи типа связи"),
 *     @OA\Property(property="connection_type_string", type="string", description="Имя записи типа связи"),
 * )
 */
class IndexResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array<string, mixed>
     */
    #[\JetBrains\PhpStorm\ArrayShape([
        'id' => 'int', 'client_id' => 'int', 'user_id' => 'int|null',
        'comment' => 'null|string', 'session_date' => 'string',
        'session_time_start' => 'string', 'session_time_end' => 'string',
        'client' => ShowResource::class,
    ])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'user_id' => $this->user_id,
            'comment' => $this->comment,
            'session_date' => $this->session_date->format('d-m-Y'),
            'session_time_start' => $this->session_time_start,
            'session_time_end' => $this->session_time_end,
            'client' => ShowResource::make($this->whenLoaded('client')),
        ];
    }
}
