<?php

declare(strict_types=1);

namespace App\Http\Resources\Api\Sessions;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

/**
 * Class ShowResource
 *
 * @mixin \App\Models\Session
 *
 * @OA\Schema(schema="SessionShowResource",
 *     type="object",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="client_id", type="integer"),
 *     @OA\Property(property="client_name", type="string"),
 *     @OA\Property(property="doctor_id", type="integer"),
 *     @OA\Property(property="comment", type="string", description="Комментарий/план на сессию"),
 *     @OA\Property(property="session_date", type="string", description="Дата встречи"),
 * )
 */
class ShowResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    #[\JetBrains\PhpStorm\ArrayShape([
        'id' => 'int', 'client_id' => 'int', 'client_name' => 'null|string',
        'user_id' => 'int|null', 'comment' => 'null|string', 'session_date' => 'string',
        'session_time_start' => 'string', 'session_time_end' => 'string',
        'repeat_count' => 'int',
    ])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'client_id' => $this->client->id,
            'client_name' => $this->client->name,
            'user_id' => $this->user_id,
            'comment' => $this->comment,
            'session_date' => $this->session_date->format('Y-m-d'),
            'session_time_start' => $this->session_time_start->format('H:i'),
            'session_time_end' => $this->session_time_end->format('H:i'),
            'repeat_count' => 0,
        ];
    }
}
