<?php

declare(strict_types=1);

namespace App\Http\Resources\Api\Sessions;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/** @mixin \App\Models\Session */
class CalendarResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'session_date' => $this->session_date,
            'session_time_start' => $this->session_time_start,
            'start' => $this->session_date->format('Y-m-d'),
            'end' => $this->session_date->format('Y-m-d'),
            'title' => $this->whenLoaded('client')?->name,
            'content' => '',
            'class' => 'leisure',
            'comment' => $this->comment,

            'name' => $this->whenLoaded('client')?->name,
            'phone' => $this->whenLoaded('client')?->phone,
            'connection_type_string' => $this->whenLoaded('client')?->connectionType?->name,
            'connection_type_link' => $this->whenLoaded('client')?->connection_type_link,
            'meeting_type' => $this->whenLoaded('client')?->meeting_type,
        ];
    }
}
