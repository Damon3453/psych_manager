<?php

declare(strict_types=1);

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\CategoryResource;

/** @mixin \App\Models\User */
class UserResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    #[\JetBrains\PhpStorm\ArrayShape([
        'id' => 'int', 'name' => 'string', 'email' => 'string',
        'phone' => 'string', 'site' => 'string',
        'categories' => CategoryResource::class,
    ])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'site' => $this->site,
            'categories' => CategoryResource::collection($this->categories),
        ];
    }
}
