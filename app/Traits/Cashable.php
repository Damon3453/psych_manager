<?php

declare(strict_types=1);

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Services\Cache\CacheManager;
use App\Repositories\BaseRepository;
use App\Contracts\RepositoryContract;

trait Cashable
{
    public static Model|Collection|self|array|int|Builder|null $result = null;

    /**
     * @return \App\Repositories\BaseRepository
     */
    public static function repository(): RepositoryContract
    {
        return app(BaseRepository::class)->setModel(static::class)->initQuery();
    }

    public static function cacheKey(): string
    {
        return app(static::class)->getTable() . '_' . debug_backtrace()[1]['function'];
    }

    /**
     * @param string $key
     *
     * @throws \Throwable
     *
     * @return mixed
     */
    public static function getByKey(string $key): mixed
    {
        return app(CacheManager::class)->get($key);
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param mixed $value
     * @param string|null $field
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null): ?\Illuminate\Database\Eloquent\Model
    {
        return static::findOrFail($value);
    }

    /**
     * Get all the models from the database.
     *
     * @param array<int, string>|string $columns
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, static>
     */
    public static function all($columns = ['*']): Collection
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->getModel()->all($columns)
        );

        return static::$result;
    }

    /**
     * Find a model by its primary key.
     *
     * @param mixed $id
     * @param array<int, string>|string $columns
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static|static[]|null
     */
    public static function find(mixed $id, array|string $columns = ['*']): Model|static|Collection|array|null
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->find($id, $columns)
        );

        return static::$result;
    }

    /**
     * Execute the query and get the first result.
     *
     * @param array|string $columns
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static|null
     */
    public static function first(array|string $columns = ['*']): Model|static|null
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(static::cacheKey(), static::$result = static::repository()->first($columns));

        return static::$result;
    }

    /**
     * Find a model by its primary key or throw an exception.
     *
     * @param mixed $id
     * @param array<int, string>|string $columns
     *
     * @throws \Exception<\Illuminate\Database\Eloquent\Model>|\Throwable
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static|static[]
     */
    public static function findOrFail(mixed $id, array|string $columns = ['*']): Model|Collection|static|array
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->findOrFail($id, $columns)
        );

        return static::repository()->findOrFail($id, $columns);
    }

    /**
     * Find a model by its primary key or return fresh model instance.
     *
     * @param mixed $id
     * @param array<int, string>|string $columns
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function findOrNew(mixed $id, array|string $columns = ['*']): Model|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->findOrNew($id, $columns)
        );

        return static::repository()->findOrNew($id, $columns);
    }

    /**
     * Get the first record matching the attributes or instantiate it.
     *
     * @param array $attributes
     * @param array $values
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function firstOrNew(array $attributes = [], array $values = []): Model|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->firstOrNew($attributes, $values)
        );

        return static::repository()->firstOrNew($attributes, $values);
    }

    /**
     * Get the first record matching the attributes or create it.
     *
     * @param array $attributes
     * @param array $values
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function firstOrCreate(array $attributes = [], array $values = []): Model|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->firstOrCreate($attributes, $values)
        );

        return static::repository()->firstOrCreate($attributes, $values);
    }

    /**
     * Create or update a record matching the attributes, and fill it with values.
     *
     * @param array $attributes
     * @param array $values
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function updateOrCreate(array $attributes, array $values = []): Model|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->updateOrCreate($attributes, $values)
        );

        return static::repository()->updateOrCreate($attributes, $values);
    }

    /**
     * Execute the query and get the first result or throw an exception.
     *
     * @param array<int, string>|string $columns
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<\Illuminate\Database\Eloquent\Model>|\Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function firstOrFail(array|string $columns = ['*']): Model|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->firstOrFail($columns)
        );

        return static::repository()->firstOrFail($columns);
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param array|string $columns
     *
     * @throws \Throwable
     *
     * @return array|\Illuminate\Database\Eloquent\Collection
     */
    public static function get(array|string $columns = ['*']): Collection|array
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(static::cacheKey(), static::$result = static::repository()->get($columns));

        return static::repository()->get($columns);
    }

    /**
     * @param array $attributes
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public static function create(array $attributes = []): Model|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->create($attributes)
        );

        return static::repository()->create($attributes);
    }

    /**
     * @param array $attributes
     * @param array $options
     *
     * @throws \Throwable
     *
     * @return int
     */
    public function update(array $attributes = [], array $options = []): int
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->update($attributes, $options)
        );

        return static::repository()->update($attributes, $options);
    }

    /**
     * @throws \Throwable
     *
     * @return mixed
     */
    public function delete(): mixed
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(static::cacheKey(), static::$result = static::repository()->delete());

        return static::repository()->delete();
    }

    /**
     * Add a basic where clause to the query.
     *
     * @param array|\Closure|\Illuminate\Database\Query\Expression|string $column
     * @param mixed|null $operator
     * @param mixed|null $value
     * @param string $boolean
     *
     * @throws \Throwable
     *
     * @return $this|Builder
     */
    public static function where(
        array|\Closure|string|\Illuminate\Database\Query\Expression $column,
        mixed $operator = null,
        mixed $value = null,
        string $boolean = 'and'
    ): Builder|static {
        if (static::getByKey(self::cacheKey())) {
            return static::getByKey(self::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->where($column, $operator, $value, $boolean)
        );

        return static::repository()->where($column, $operator, $value, $boolean);
    }

    /**
     * @param $relations
     * @param $callback
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public static function with($relations, $callback = null): Builder|static
    {
        if (static::getByKey(static::cacheKey())) {
            return static::getByKey(static::cacheKey());
        }
        app(CacheManager::class)->put(
            static::cacheKey(),
            static::$result = static::repository()->with($relations, $callback)
        );

        return static::$result;
    }
}
