<?php

declare(strict_types=1);

namespace App\Grants;

use Laravel\Passport\Bridge\User;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\DataValueObjects\PhoneValue;

/**
 * Password grant class.
 */
class PhoneGrant extends AbstractGrant
{
    public function __construct(
        AccessTokenRepositoryInterface $accessTokenRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository,
    ) {
        $this->setAccessTokenRepository($accessTokenRepository);
        $this->setRefreshTokenRepository($refreshTokenRepository);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ClientEntityInterface $client
     *
     * @throws OAuthServerException
     *
     * @return UserEntityInterface
     */
    protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client): UserEntityInterface
    {
        $phone = $this->getRequestParameter('phone', $request);

        if (is_null($phone)) {
            throw OAuthServerException::invalidRequest($this->getIdentifier());
        }

        $code = $this->getRequestParameter('code', $request);

        if (is_null($code)) {
            throw OAuthServerException::invalidRequest('code');
        }

        $user = $this->getUserEntityByPhone(
            $phone,
            $code,
            $client
        );

        if ($user instanceof UserEntityInterface === false) {
            $this->getEmitter()->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));
            throw OAuthServerException::invalidGrant();
        }

        return $user;
    }

    /**
     * @inheritdoc
     *
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     */
    public function respondToAccessTokenRequest(
        ServerRequestInterface $request,
        ResponseTypeInterface $responseType,
        \DateInterval $accessTokenTTL
    ): ResponseTypeInterface {
        // Validate request
        $client = $this->validateClient($request);
        $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
        $user = $this->validateUser($request, $client);

        // Finalize the requested scopes
        $finalizedScopes = $this->scopeRepository->finalizeScopes(
            $scopes,
            $this->getIdentifier(),
            $client,
            $user->getIdentifier()
        );

        // Issue and persist new access token
        $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
        $this->getEmitter()->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
        $responseType->setAccessToken($accessToken);

        // Issue and persist new refresh token if given
        $refreshToken = $this->issueRefreshToken($accessToken);

        if ($refreshToken !== null) {
            $this->getEmitter()->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
            $responseType->setRefreshToken($refreshToken);
        }

        return $responseType;
    }

    /**
     * @param string $phone
     * @param string $code
     * @param ClientEntityInterface $clientEntity
     *
     * @return User
     */
    public function getUserEntityByPhone(string $phone, string $code, ClientEntityInterface $clientEntity): User
    {
        $provider = $clientEntity->provider ?: config('auth.guards.api.provider');

        if (is_null($model = config('auth.providers.' . $provider . '.model'))) {
            throw new \RuntimeException('Unable to determine authentication model from configuration.');
        }

        $phone = PhoneValue::make($phone)->toNative();

        /** @var \App\Models\User $user */
        $user = $model::query()->firstWhere('phone', $phone);
        if (!$user) {
            throw \App\Exceptions\InvalidParameterException::make('Не найден пользователь.');
        }
        $phoneVerify = $user->userPhoneVerifies()->latest()->first();

        if (!\App::isLocal()) {
            switch (true) {
                case $phoneVerify->code !== $code:
                case !$phoneVerify->isValid():
                    throw \App\Exceptions\InvalidParameterException::make('Неверный код.');
                    // case !$user->hasVerifiedPhone():
                    //     throw \App\Exceptions\InvalidParameterException::make('Телефон не подтверждён.');
                    // case $user->hasVerifiedPhone():
                    //     throw \App\Exceptions\InvalidParameterException::make('Телефон уже подтверждён.');
                default:
                    break;
            }
        }

        return new User($user->getAuthIdentifier());
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): string
    {
        return 'phone';
    }
}
