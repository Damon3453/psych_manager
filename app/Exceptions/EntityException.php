<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class EntityException extends HttpException
{
    /**
     * The stores.
     *
     * @var array<array-key, mixed>
     */
    protected array $data = [];

    public function __construct(
        int $statusCode = 501,
        string $message = '',
        \Throwable $previous = null,
        array $headers = [],
        int $code = 0
    ) {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    final public static function make(string $message): static
    {
        return new static($message, ...func_get_args());
    }

    /**
     * Get the store data
     *
     * @return array<array-key, mixed>
     */
    final public function getData(): array
    {
        return $this->data;
    }

    /**
     * Set the store data.
     *
     * @param array<array-key, mixed> $data
     *
     * @return $this
     */
    final public function setData(array $data): static
    {
        $this->data = $data;

        return $this;
    }
}
