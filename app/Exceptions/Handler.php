<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Notifications\DiscordNotification;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $e
     *
     * @throws \Throwable
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function render(
        $request,
        \Throwable $e
    ): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
    {
        if ($e instanceof \Illuminate\Database\QueryException) {
            app(DiscordNotification::class)->setExceptionsEmbedFields($e)->toDiscord();
            return parent::render($request, $e);
        }
        if ($e instanceof \Illuminate\Auth\AuthenticationException && $e->getMessage() === 'Unauthenticated.') {
            return \ApiBaseResponder::response([], 'Not Found', 404);
        }
        if ($e instanceof \TypeError) {
            return \ApiBaseResponder::response(
                [],
                $e->getMessage(),
                \Illuminate\Http\JsonResponse::HTTP_UNSUPPORTED_MEDIA_TYPE
            );
        }

        return parent::render($request, $e);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        // \Notification::send($request->user(), new DiscordNotification($e));
        if (\App::isProduction()) {
            $this->renderable(function (EntityException $e, Request $request) {
                if ($request->wantsJson() || $request->is('api/*')) {
                    app(DiscordNotification::class)
                        ->setExceptionsEmbedFields($e)
                        ->pushEmbed()
                        ->toDiscord();

                    return \ApiBaseResponder::response([], $e->getMessage(), $e->getStatusCode());
                }
            });
            $this->renderable(function (NotFoundHttpException|EntityNotFoundException $e, Request $request) {
                if ($request->wantsJson() || $request->is('api/*')) {
                    app(DiscordNotification::class)
                        ->setExceptionsEmbedFields($e)
                        ->pushEmbed()
                        ->toDiscord();

                    return \ApiBaseResponder::response([], 'Запись не найдена', 404);
                }
            });
        }
    }
}
