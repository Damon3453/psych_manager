<?php

declare(strict_types=1);

namespace App\Pipes\Filters\Client;

use App\Pipes\Filters\Filterable;

class MeetingType
{
    public function handle(Filterable $filterable, \Closure $next)
    {
        $query = $filterable->query;
        /** @var \App\DataObjects\Filters\SessionFilterData $filters */
        $filters = $filterable->filters;
        $meetingType = !empty($filters->meetingType) || $filters->meetingType === '0';

        $query->when(
            $meetingType,
            static fn() => $query->where('meeting_type', $filters->meetingType)
        );

        return $next(Filterable::make($query, $filterable->filters));
    }
}
