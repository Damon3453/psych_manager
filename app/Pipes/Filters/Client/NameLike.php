<?php

declare(strict_types=1);

namespace App\Pipes\Filters\Client;

use App\Pipes\Filters\Filterable;

class NameLike
{
    public function handle(Filterable $filterable, \Closure $next)
    {
        $query = $filterable->query;
        /** @var \App\DataObjects\Filters\SessionFilterData $filters */
        $filters = $filterable->filters;

        $query->when(
            $filters->userName ?? false,
            static fn() => $query->where('name', 'like', "%$filters->userName%")
        );

        return $next(Filterable::make($query, $filterable->filters));
    }
}
