<?php

declare(strict_types=1);

namespace App\Pipes\Filters\Session;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Pipeline;
use App\Pipes\Filters\Filterable;
use App\Pipes\Filters\Client\NameLike;

class ClientUserName
{
    public function handle(Filterable $filterable, \Closure $next)
    {
        $query = $filterable->query;
        /** @var \App\DataObjects\Filters\SessionFilterData $filters */
        $filters = $filterable->filters;

        $query->when(
            $filters->userName ?? false,
            static fn() => $query->whereHas(
                'client',
                static fn(Builder $q) => Pipeline::send(
                    Filterable::make($q, $filterable->filters)
                )->through(
                    NameLike::class
                )->thenReturn()->query()
            )
        );

        return $next(Filterable::make($query, $filterable->filters));
    }
}
