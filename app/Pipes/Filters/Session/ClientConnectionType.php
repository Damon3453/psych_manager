<?php

declare(strict_types=1);

namespace App\Pipes\Filters\Session;

use App\Pipes\Filters\Filterable;
use Illuminate\Database\Eloquent\Builder;

class ClientConnectionType
{
    public function handle(Filterable $filterable, \Closure $next)
    {
        $query = $filterable->query;
        /** @var \App\DataObjects\Filters\SessionFilterData $filters */
        $filters = $filterable->filters;
        $connectionType = !empty($filters->connectionType) || $filters->connectionType === '0';

        $query->when(
            $connectionType,
            static fn() => $query->whereRelation(
                relation: 'client',
                column: static fn(Builder $q) => $q->whereRelation(
                    'connectionType',
                    'id',
                    $filters->connectionType
                )
            )
        );

        return $next(Filterable::make($query, $filterable->filters));
    }
}
