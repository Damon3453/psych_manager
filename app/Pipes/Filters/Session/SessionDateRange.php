<?php

declare(strict_types=1);

namespace App\Pipes\Filters\Session;

use App\Pipes\Filters\Filterable;

class SessionDateRange
{
    public function handle(Filterable $filterable, \Closure $next)
    {
        $query = $filterable->query;
        /** @var \App\DataObjects\Filters\SessionFilterData $filters */
        $filters = $filterable->filters;
        $dateRange = [];
        if (!empty($filters->dateRange)) {
            $dateRange = resolveFilterDateRange($filters->dateRange);
        }

        $query->when(
            !empty($dateRange),
            static fn() => $query->whereBetween('session_date', $filters->dateRange)
        );

        return $next(Filterable::make($query, $filterable->filters));
    }
}
