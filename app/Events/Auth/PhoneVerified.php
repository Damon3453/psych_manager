<?php

declare(strict_types=1);

namespace App\Events\Auth;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PhoneVerified
{
    use Dispatchable;
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\User $user
     *
     * @return void
     */
    public function __construct(public readonly \App\Contracts\MustVerifyPhone $user)
    {
    }
}
