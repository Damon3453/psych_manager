<?php

declare(strict_types=1);

namespace App\Events\Auth;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Registered
{
    use Dispatchable;
    use SerializesModels;

    /**
     * The authenticated user.
     *
     * @var \App\Models\User
     */
    public \Illuminate\Contracts\Auth\Authenticatable $user;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\User $user
     *
     * @return void
     */
    public function __construct(\Illuminate\Contracts\Auth\Authenticatable $user)
    {
        $this->user = $user;
    }
}
