<?php

declare(strict_types=1);

namespace App\DataValueObjects;

use App\Helpers\PhoneHelper;

class PhoneValue
{
    public function __construct(
        public ?string $phone = null,
    ) {
        $this->phone = PhoneHelper::clear($phone);
    }

    public static function make(?string $phone): static
    {
        return new static($phone);
    }

    public function toNative(): ?string
    {
        return $this->phone;
    }

    public function isEqual(self $phone): bool
    {
        return $this->toNative() === $phone->toNative();
    }

    public function isEmpty(): bool
    {
        return $this->toNative() === null;
    }
}
