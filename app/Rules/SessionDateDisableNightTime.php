<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\{DataAwareRule, InvokableRule};
use Illuminate\Support\Carbon;

class SessionDateDisableNightTime implements DataAwareRule, InvokableRule
{
    /**
     * All the data under validation.
     *
     * @var array $data
     */
    protected array $data = [];

    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     *
     * @return void
     */
    public function __invoke($attribute, $value, $fail): void
    {
        $carbonTimeStart = Carbon::createFromTimeString("{$this->data['session_date']} $value");
        $night = "{$carbonTimeStart->format('Y:m:d')} 00:00:00";
        $morning = "{$carbonTimeStart->format('Y:m:d')} 06:00:00";
        if (!$carbonTimeStart->greaterThanOrEqualTo($night) || $carbonTimeStart->lessThanOrEqualTo($morning)) {
            $fail('Время сессии не может быть установлено между 00:00 и 06:00 часов');
        }
    }

    /**
     * @inheritdoc
     */
    public function setData($data): static
    {
        $this->data = $data;

        return $this;
    }
}
