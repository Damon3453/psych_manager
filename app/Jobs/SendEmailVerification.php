<?php

declare(strict_types=1);

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Notifications\DiscordNotification;
use App\Models\User;

class SendEmailVerification implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public int $tries = 3;

    public int $maxExceptions = 3;

    public int $timeout = 5;

    /*
     * Create a new job instance.
     */
    public function __construct(public readonly User $user)
    {
        $this->onQueue('emails');
        $this->delay(3);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        event(new \App\Events\Auth\Registered($this->user));
    }

    /*
     * Handle a job failure.
     */
    public function failed(\Throwable $exception): void
    {
        app(DiscordNotification::class)->setExceptionsEmbedFields($exception)->toDiscord();
    }
}
