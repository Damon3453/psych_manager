<?php

declare(strict_types=1);

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Notifications\DiscordNotification;
use App\Services\SmsAeroService;

class SendSms implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public int $tries = 3;

    public int $maxExceptions = 3;

    public int $timeout = 5;

    /**
     * Create a new job instance.
     *
     * @param \App\DataValueObjects\PhoneValue|string $phone
     * @param string $text
     */
    public function __construct(
        public \App\DataValueObjects\PhoneValue|string $phone,
        public string $text
    ) {
        $this->onQueue('phones');

        if (is_string($this->phone)) {
            $this->phone = \App\DataValueObjects\PhoneValue::make($this->phone);
        }
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle(): void
    {
        // TODO: via notification
        // event(new \App\Events\Auth\PhoneVerified($this->user));
        // (new VerifyPhone())->toSms();
        // Notification::route(SmsChannel::class, $this->user->phone)->notify(new VerifyPhone());
        if (!app()->isProduction()) {
            app(SmsAeroService::class)->testSend($this->phone, 'From job class');
        } else {
            app(SmsAeroService::class)->handle($this->phone, $this->text);
        }
    }

    /*
     * Handle a job failure.
     */
    public function failed(\Throwable $exception): void
    {
        app(DiscordNotification::class)->setExceptionsEmbedFields($exception)->toDiscord();
    }
}
