<?php

declare(strict_types=1);

namespace App\Builders;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Pipeline;
use App\Pipes\Filters\Filterable;
use App\Pipes\Filters\Session\{ClientConnectionType, SessionDateRange, ClientMeetingType, ClientUserName};
use App\Models\Session;

/**
 * @template TModelClass of \App\Models\User
 * @template TModelClass of \App\Models\Session
 *
 * @extends Builder<\App\Models\User>
 * @extends Builder<\App\Models\Session>
 */
class FilterBuilder extends Builder
{
    /**
     * @param array<string, mixed> $pagination
     *
     * @return Builder
     */
    public function withPagination(array $pagination = []): Builder
    {
        $offset = (($pagination['page'] ?? 1) - 1) * ($pagination['limit'] ?? 50);

        $this->offset($offset)->limit($pagination['limit'] ?? 50);

        return $this;
    }

    /**
     * @param array<string, mixed> $sort
     *
     * @return Builder
     */
    public function sort(array $sort = []): Builder
    {
        $direction = filter_var($sort['order'], FILTER_VALIDATE_BOOLEAN) ? 'asc' : 'desc';
        if ($sort['field'] !== 'session_date_users') {
            $this->orderBy($this->resolveSortField($sort['field'] ?? 'id'), $direction);
        } else {
            // $this->whereHas('sessions', function ($q) use ($order) {
            //     return $q->orderBy('session_date', $order)->dd();
            // });
            // dd(
            //     $this->select('clients.id')->join('sessions', 'sessions.client_id', '=', 'clients.id')
            //         ->select('sessions.session_date')
            //         ->orderBy('sessions.session_date', $order);
            // );
            // dd(
            $this->orderBy(
                Session::select(['session_date'])
                    ->whereColumn('sessions.client_id', 'clients.id')
                    ->latest('session_date')
                    ->take(1),
                $direction
            );
            // );
            // $this->whereHas('sessions', fn($q) => $q->orderBy('session_date', $order));
            // $this->orderBy(
            //     $this-whereHas('sessions', fn($q) => $q->latest('session_date')),
            //     $order
            // );
        }

        return $this;
    }

    /**
     * @param string $field
     *
     * @return string
     */
    public function resolveSortField(string $field): string
    {
        return match ($field) {
            'id' => 'id',
            default => $field,
        };
    }

    /**
     * @param array<string, mixed> $filters
     *
     * @return Builder
     */
    public function clientFilters(array $filters = []): Builder
    {
        $meetingType = $filters['meeting_type'] || $filters['meeting_type'] === '0';
        $categoryId = $filters['category_id'] || $filters['category_id'] === '0';
        $connectionType = $filters['connection_type'] || $filters['connection_type'] === '0';
        $dateRange = [];
        if (!empty($filters['date_range'])) {
            $dateRange = resolveFilterDateRange($filters['date_range']);
        }

        $this
            ->when($meetingType, fn() => $this->where('meeting_type', $filters['meeting_type']))
            ->when($categoryId, fn() => $this->where('category_id', $filters['category_id']))
            ->when(
                $filters['name'] ?? false,
                fn() => $this->where('name', 'like', "%{$filters['name']}%")
            )
            ->when(
                $filters['email'] ?? false,
                fn() => $this->where('email', 'like', "%{$filters['email']}%")
            )
            ->when(
                $connectionType,
                fn() => $this->whereRelation('connectionType', 'id', $filters['connection_type'])
            )
            ->when(
                $filters['phone'] ?? false,
                fn() => $this->where('phone', 'like', "%{$filters['phone']}%")
            )
            ->when(
                !empty($dateRange),
                fn() => $this->whereHas(
                    'sessions',
                    static fn($q) => $q->whereBetween('session_date', $dateRange)
                )
            );

        return $this;
    }

    /**
     * @param \App\DataObjects\Filters\SessionFilterData $filters
     *
     * @return Builder
     */
    public function sessionFilters(\Spatie\LaravelData\Contracts\DataObject $filters): Builder
    {
        return Pipeline::send(
            Filterable::make($this, $filters)
        )->through([
            SessionDateRange::class,
            ClientMeetingType::class,
            ClientConnectionType::class,
            ClientUserName::class,
        ])->thenReturn()->query();
    }
}
