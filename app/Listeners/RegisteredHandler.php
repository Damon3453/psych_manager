<?php

declare(strict_types=1);

namespace App\Listeners;

use Illuminate\Contracts\Auth\MustVerifyEmail;

// use Illuminate\Contracts\Queue\ShouldQueue;
// use Illuminate\Queue\InteractsWithQueue;

class RegisteredHandler // implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param \App\Events\Auth\Registered $event
     *
     * @return void
     */
    public function handle(\App\Events\Auth\Registered $event): void
    {
        if ($event->user instanceof MustVerifyEmail && !$event->user->hasVerifiedEmail()) {
            $event->user->sendEmailVerificationNotification();
        }
    }
}
