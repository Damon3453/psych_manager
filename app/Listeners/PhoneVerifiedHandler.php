<?php

declare(strict_types=1);

namespace App\Listeners;

class PhoneVerifiedHandler
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\Auth\PhoneVerified $event
     *
     * @return void
     */
    public function handle(\App\Events\Auth\PhoneVerified $event): void
    {
        if ($event->user instanceof \App\Contracts\MustVerifyPhone && !$event->user->hasVerifiedPhone()) {
            $event->user->sendPhoneVerificationNotification();
        }
    }
}
