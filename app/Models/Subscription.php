<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use App\DataObjects\Payments\YooKassa\YooKassaSuccessResponseData;

class Subscription extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'payment_method_id',
        'status',
        'period',
        'cancel_date',
        'amount',
        'response',
        'created_at',
    ];

    protected $casts = [
        'response' => YooKassaSuccessResponseData::class,
        'cancel_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y',
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeExpired(Builder $query): void
    {
        $query->whereDate('cancel_date', '<', \Carbon\Carbon::today());
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeActive(Builder $query): void
    {
        $query->where('status', 'succeeded');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
