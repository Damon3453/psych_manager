<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @template TModel of \Illuminate\Database\Eloquent\Model
 *
 * @extends \Illuminate\Database\Eloquent\Model
 */
abstract class BaseModel extends Model
{
    /**
     * @return Builder
     */
    final public function queryModel(): Builder
    {
        return $this->newQuery();
    }

    /**
     * @return Builder
     */
    final public static function q(): Builder
    {
        return static::query();
    }

    /**
     * @return static
     */
    final public static function n(): self
    {
        return new static();
    }
}
