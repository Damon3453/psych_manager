<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class UserEmailVerify extends Model
{
    protected $fillable = [
        'email',
        'hash',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot(): void
    {
        parent::boot();

        static::creating(static function (self $model): void {
            $model->setAttribute('hash', $model->generateHash());
        });
    }

    /**
     * @return string
     */
    public function generateHash(): string
    {
        // $token = hash_hmac('sha256', Uuid::uuid4() . $this->user->email, config('app.key'), true);

        // return strtr(base64_encode($token), '+/=', '._-');
        return sha1($this->user->getEmailForVerification() . $this->user->getAuthIdentifier());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
