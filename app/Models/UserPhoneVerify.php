<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPhoneVerify extends Model
{
    public const EXPIRATION_TIME = 15; // minutes

    public const TYPE_REGISTER = 0;

    public const TYPE_LOGIN = 1;

    public const TYPE_PASS_RESET = 2;

    protected $fillable = [
        'phone',
        'code',
        'is_used',
        'type',
    ];

    protected $casts = ['is_used' => 'bool'];

    /**
     * @param array $attributes
     *
     * @throws \Exception
     */
    public function __construct(array $attributes = [])
    {
        if (!isset($this->code)) {
            $this->setAttribute('code', $this->generateCode());
        }

        parent::__construct($attributes);
    }

    /**
     * True if the token is not used nor expired
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return !$this->isUsed() && !$this->isExpired();
    }

    /**
     * Is the current code used
     *
     * @return bool
     */
    public function isUsed(): bool
    {
        return $this->is_used;
    }

    /**
     * Is the current token expired
     *
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->created_at->diffInMinutes(\Carbon\Carbon::now()) > static::EXPIRATION_TIME;
    }

    /**
     * Generate a {$codeLength} digits code
     *
     * @param int $codeLength
     *
     * @throws \Exception
     *
     * @return int
     */
    public function generateCode(int $codeLength = 6): int
    {
        $min = 10 ** ($codeLength - 1);
        $max = $min * 10 - 1;

        return random_int($min, $max);
    }

    /**
     * Mark the given code as used.
     *
     * @return bool
     */
    public function markCodeAsUsed(): bool
    {
        return $this->forceFill([
            'is_used' => true,
        ])->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
