<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Builder,
    Casts\Attribute,
    Factories\HasFactory,
};
use Illuminate\Contracts\Cache\Store;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Bkwld\Cloner\Cloneable;
use App\Builders\FilterBuilder;
use App\Models\Scopes\SessionUserScope;

/**
 * Class Session
 *
 * @template TModel of \App\Models\BaseModel
 *
 * @extends \App\Models\BaseModel<TModel>
 *
 * @property int $id
 * @property int $client_id
 * @property int $doctor_id
 * @property string $comment
 * @property \Carbon\Carbon $session_date
 * @property \Carbon\Carbon $session_time_start
 * @property \Carbon\Carbon $session_time_end
 * @property User $user
 */
class Session extends BaseModel
{
    use Cloneable;
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $table = 'sessions';

    /**
     * @inheritdoc
     */
    protected $fillable = ['comment', 'session_date', 'session_time_start', 'session_time_end'];

    protected $casts = [
        'session_date' => 'datetime:d-m-Y',
        'session_time_start' => 'datetime',
        'session_time_end' => 'datetime',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted(): void
    {
        static::addGlobalScope(new SessionUserScope(\Auth::id() ?? User::first()->id));
        $store = app(Store::class);

        static::created(static fn() => $store->flush());
        static::updated(static fn() => $store->flush());
        static::deleted(static fn() => $store->flush());
    }

    /**
     * @param QueryBuilder $query
     *
     * @return FilterBuilder<\App\Models\Session>
     */
    public function newEloquentBuilder($query): FilterBuilder
    {
        return new FilterBuilder($query);
    }

    /**
     * @throws \Exception
     *
     * @return Attribute
     */
    public function sessionDateCalendar(): Attribute
    {
        return Attribute::make(
            get: fn($value) => \Carbon\Carbon::parse($this->session_date)->format('Y-m-d H:i')
        );
    }

    /**
     * @param ?string $comment
     *
     * @return $this
     */
    public function setComment(string $comment = null): static
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @param string $sessionDate
     *
     * @return $this
     */
    public function setSessionDate(string $sessionDate): static
    {
        $this->session_date = $sessionDate;

        return $this;
    }

    /**
     * @param string $timeStart
     *
     * @return $this
     */
    public function setSessionTimeStart(string $timeStart): static
    {
        $this->session_time_start = $timeStart;

        return $this;
    }

    /**
     * @param string $timeEnd
     *
     * @return $this
     */
    public function setSessionTimeEnd(string $timeEnd): static
    {
        $this->session_time_end = $timeEnd;

        return $this;
    }

    /**
     * -------------------------------------
     * RELATIONS
     * -------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\Client, \App\Models\Session>
     */
    public function client(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\User, \App\Models\Session>
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
