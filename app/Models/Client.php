<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Query\Builder as QueryBuilder;
use App\Builders\FilterBuilder;
use App\Models\Scopes\SessionUserScope;

/**
 * @template TModel of \App\Models\User
 *
 * @extends \App\Models\User
 */
class Client extends User
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $table = 'clients';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name', 'email', 'phone', 'birthday_date', 'gender',
        'meeting_type', 'connection_type', 'connection_type_link', 'curator_contacts',
    ];

    /**
     * @inheritdoc
     */
    protected static function booted(): void
    {
        static::addGlobalScope(new SessionUserScope(\Auth::id() ?? User::first()->id));
    }

    /**
     * @param QueryBuilder $query
     *
     * @return FilterBuilder<\App\Models\Client>
     */
    public function newEloquentBuilder($query): FilterBuilder
    {
        return new FilterBuilder($query);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Session>
     */
    public function sessions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Session::class, 'client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function latestSession(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Session::class, 'client_id', 'id')->latest('session_date');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function connectionType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ConnectionType::class, 'connection_type_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function therapy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ClientTherapy::class, 'client_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
