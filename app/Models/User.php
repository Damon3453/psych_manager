<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

use Laravel\Passport\HasApiTokens;

use App\Contracts\MustVerifyPhone as MustVerifyPhoneContract;
use App\Notifications\VerifyEmail;
use App\Traits\MustVerifyPhone;
use App\DataValueObjects\PhoneValue;

/**
 * Class User
 *
 * @package App\Models
 *
 * @template TModel of \Illuminate\Database\Eloquent\Model
 * @extends Authenticatable
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $password
 *
 * Relation:
 * @property Session[]|Collection $sessions
 * @property ConnectionType[]|Collection $connectionType
 *
 * Scopes:
 * @method Builder withFilters()
 * @method Builder paginationApi()
 * @method Builder withTrashed()
 */
class User extends Authenticatable implements MustVerifyEmailContract, MustVerifyPhoneContract
{
    use MustVerifyEmail;
    use MustVerifyPhone;
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $table = 'users';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'site', 'email_verified_at', 'phone_verified_at',
    ];

    /**
     * @inheritdoc
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @inheritdoc
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    // protected $with = ['subscription'];

    /**
     * Always encrypt password when it is updated.
     *
     * @return Attribute
     */
    // protected function password(): Attribute
    // {
    //     return Attribute::make(
    //         set: static fn($value) => Hash::make((string) $value, [PASSWORD_DEFAULT])
    //     );
    // }

    /**
     * @return Attribute
     */
    protected function phone(): Attribute
    {
        return Attribute::make(
            set: static fn($value) => PhoneValue::make($value)->toNative()
        );
    }

    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new VerifyEmail());
    }

    public function routeNotificationForDiscord()
    {
        return config('services.discord.notification_channel_id');
    }

    /*
     * -------------------------------------
     * RELATIONS
     * -------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Client::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Session>
     */
    public function sessions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Session::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filters(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UserFilter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userEmailVerify(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(UserEmailVerify::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPhoneVerifies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UserPhoneVerify::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscription(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Subscription::class);
    }
}
