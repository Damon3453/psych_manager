<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\DataObjects\Payments\YooKassa\PaymentMethod\YooKassaPaymentMethodResponseData;

class PaymentNotification extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $fillable = [
        'id',
        'event',
        'status',
        'amount_paid',
        'amount_income',
        'recipient_account_id',
        'recipient_gateway_id',
        'payment_method',
        'captured_at',
        'paid',
        'refundable',
        'authorization_details',
        'cancellation_details',
        'created_at',
    ];

    protected $casts = [
        'payment_method' => YooKassaPaymentMethodResponseData::class,
        'captured_at' => 'datetime:d-m-Y',
        'created_at' => 'datetime:d-m-Y',
        'paid' => 'bool',
        'refundable' => 'bool',
        'authorization_details' => 'array',
        'cancellation_details' => 'array',
    ];
}
