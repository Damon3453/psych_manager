<?php

declare(strict_types=1);

namespace App\Models;

class UserFilter extends BaseModel
{
    /**
     * @inheritdoc
     */
    protected $casts = [
        'value' => 'json',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
