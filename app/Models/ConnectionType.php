<?php

declare(strict_types=1);

namespace App\Models;

/**
 * Class ConnectionType
 *
 * @property int $id
 * @property string $name
 */
class ConnectionType extends BaseModel
{
    /**
     * @inheritdoc
     */
    protected $table = 'connection_types';

    /**
     * @inheritdoc
     */
    protected $fillable = ['name'];

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * -------------------------------------
     * RELATIONS
     * -------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Client::class, 'connection_type_id', 'id');
    }
}
