<?php

declare(strict_types=1);

namespace App\Enums\Payments;

enum YooKassaPaymentStatusEnum: string
{
    case PENDING = 'pending';

    case SUCCEEDED = 'succeeded';

    case CANCELED = 'canceled';
}
