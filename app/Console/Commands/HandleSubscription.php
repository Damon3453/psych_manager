<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Contracts\PaymentContract;
use App\Models\{Subscription, PaymentProvider};

class HandleSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payments:handle-subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка запросов в кассу на списание платы за подписку';

    /**
     * Execute the console command.
     *
     * @param \App\Services\Payments\PaymentManager $factory
     */
    public function handle(PaymentContract $factory): void
    {
        $provider = PaymentProvider::query()->firstWhere('name', 'YooKassa');
        $service = $factory->from($provider->name);

        try {
            DB::beginTransaction();

            Subscription::query()->active()->expired()->get()->each(
                static function (Subscription $subscription) use ($service) {
                    $response = $service->subscription($subscription)->handleSubscription();
                    // todo:
                    if ($response->status === 'some fail') {
                        $service->subscription($subscription)->cancelSubscription();
                    }
                }
            );

            // Subscription::query()->active()->expired()->lazy(
            //     config('database.collection_settings.lazy.chunk_size')
            // )->each(
            //     static fn(Subscription $subscription) => $service->subscription($subscription)->handleSubscription()
            // );

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
        }
    }
}
