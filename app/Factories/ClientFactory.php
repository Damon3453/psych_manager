<?php

declare(strict_types=1);

namespace App\Factories;

use App\Services\Core\ClientModel;
use App\Dto\ClientDto;

class ClientFactory extends BaseFactory
{
    public function __construct()
    {
        $this->dto = new ClientDto();
    }

    /**
     * @inheritdoc
     */
    public function make(): \App\Contracts\ServiceModelContract
    {
        return new ClientModel($this->dto);
    }
}
