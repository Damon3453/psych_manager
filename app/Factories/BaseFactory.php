<?php

declare(strict_types=1);

namespace App\Factories;

use App\Contracts\{CommonDtoContract, CommonFactoryContract, FormRequestContract};

abstract class BaseFactory implements CommonFactoryContract
{
    protected CommonDtoContract $dto;

    protected \App\Contracts\ServiceModelContract $coreModel;

    final public function fromRequest(FormRequestContract $request): static
    {
        $this->dto = $request->toDto();

        return $this;
    }

    /**
     * @return \App\Contracts\ServiceModelContract
     */
    abstract public function make(): \App\Contracts\ServiceModelContract;
}
