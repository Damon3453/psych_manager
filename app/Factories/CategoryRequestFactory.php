<?php

declare(strict_types=1);

namespace App\Factories;

use App\Services\Core\CategoryRequestModel;

class CategoryRequestFactory extends BaseFactory
{
    /**
     * @inheritdoc
     */
    public function make(): \App\Contracts\ServiceModelContract
    {
        return new CategoryRequestModel($this->dto);
    }
}
