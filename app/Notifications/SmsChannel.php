<?php

declare(strict_types=1);

namespace App\Notifications;

use App\DataValueObjects\PhoneValue;

class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param \App\Models\User $notifiable
     * @param \App\Notifications\VerifyPhone $notification
     *
     * @throws \Exception
     */
    public function send(\App\Models\User $notifiable, VerifyPhone $notification): void
    {
        $notification->toSms()
            ->testSend(
                PhoneValue::make($notifiable->phone),
                'From channel class'
            );
    }
}
