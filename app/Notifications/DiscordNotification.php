<?php

declare(strict_types=1);

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Illuminate\Database\QueryException;

class DiscordNotification extends Notification
{
    use Queueable;

    private string $url;

    private array $params;

    private array $embeds = [];

    /**
     * @see https://gist.github.com/thomasbnt/b6f455e2c7d743b796917fa3c205f812#file-code_colors_discordjs-md
     */
    private int|string $color = 5763719;

    private string $icon;

    private array $requestBodyFields = [];

    private QueryException|\Throwable $e;

    /**
     * Create a new notification instance.
     *
     * @param string $discordWebhook
     *
     * @return void
     */
    public function __construct(string $discordWebhook)
    {
        $this->url = $discordWebhook;
        $this->icon = asset('i/icons/check.png');
        $this->e = new \Exception('Empty exception.');
    }

    public function setException(QueryException|\Throwable $e): static
    {
        $this->e = $e;

        return $this;
    }

    public function setExceptionsEmbedFields(QueryException|\Throwable $e): static
    {
        $codeValue = $this->e instanceof HttpExceptionInterface ?
            $this->e->getStatusCode() :
            '418';

        $this->requestBodyFields = [
            ['name' => 'Type', 'value' => $e::class],
            ['name' => 'Code', 'value' => $codeValue],
            ['name' => 'Message', 'value' => $e->getMessage()],
            ['name' => 'File', 'value' => $e->getFile()],
            ['name' => 'Line', 'value' => $e->getLine()],
        ];

        $this->pushEmbed();

        return $this;
    }

    /**
     * @return $this
     */
    public function pushEmbed(): static
    {
        $this->embeds[] = [
            'image' => [
                'url' => $this->icon,
            ],
            'thumbnail' => [
                'url' => $this->icon,
            ],
            'timestamp' => \Carbon\Carbon::now(),
            'title' => 'Alert',
            // 'description' => "*{$this->params[0]}*",
            'author' => ['name' => config('app.name')],
            'footer' => [
                // 'text' => 'footer text',
                'icon_url' => $this->icon,
            ],
            'fields' => $this->requestBodyFields,
            'color' => $this->color,
        ];

        return $this;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via(mixed $notifiable): array
    {
        return [];
    }

    /**
     * @param ...$params
     *
     * @return $this
     */
    public function setData(...$params): static
    {
        $this->params = $params;

        return $this;
    }

    public function setFields(array $fields): static
    {
        // todo: dto
        $this->requestBodyFields = $fields;

        return $this;
    }

    /**
     * @param int|string $color
     *
     * @return $this
     */
    public function setColor(int|string $color): static
    {
        $this->color = $color;

        return $this;
    }

    public function setDangerIcon(): static
    {
        $this->icon = asset('i/icons/delete.svg');

        return $this;
    }

    /**
     * @return void
     */
    public function toDiscord(): void
    {
        $data = [
            'content' => '@developers: Env: ' . \App::environment(),
            'username' => config('app.name'),
            // 'allowed_mentions' => [
            //     "parse" => ["everyone"]
            // ],
            'embeds' => $this->embeds,
        ];

        \Http::post($this->url, $data); // ->throwIfClientError();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray(mixed $notifiable): array
    {
        return [];
    }
}
