<?php

declare(strict_types=1);

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;

class DiscordNotificationChannel extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param object $notifiable
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [DiscordChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * duplicated documentation:
     *
     * @see https://discord.com/developers/docs/resources/channel#embed-object
     * @see https://github.com/discord/discord-api-docs/blob/main/docs/resources/Channel.md#embed-object
     *
     * @param mixed $notifiable
     */
    public function toDiscord($notifiable)
    {
        return DiscordMessage::create('HEY EBEN!')
            ->body('body + *body kursiv* + **body jir**')
            ->embed([
                'title' => 'Notification Title',
                'description' => 'Notification Description',
                'color' => 0xFF0000,
                'fields' => [
                    [
                        'name' => 'Field 1',
                        'value' => 'Value 1',
                    ],
                    [
                        'name' => 'Field 2',
                        'value' => 'Value 2',
                    ],
                ],
            ]);
    }
}
