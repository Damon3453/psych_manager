<?php

declare(strict_types=1);

namespace App\Dto;

use JetBrains\PhpStorm\Pure;
use ReturnTypeWillChange;

abstract class BaseDto implements \JsonSerializable
{
    /**
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        foreach ($params as $key => $param) {
            if (property_exists($this, $key)) {
                $this->$key = $param;
            }
        }
    }

    /**
     * @param array $params
     *
     * @return static
     */
    final public static function make(array $params = []): static
    {
        return new static($params);
    }

    /**
     * @return array
     */
    final public function toArray(): array
    {
        return (array) $this;
    }

    /**
     * @inheritdoc
     */
    #[\ReturnTypeWillChange] #[Pure]
    final public function jsonSerialize()
    {
        return $this->toArray();
    }
}
