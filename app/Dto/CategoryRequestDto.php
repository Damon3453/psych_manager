<?php

declare(strict_types=1);

namespace App\Dto;

use App\Contracts\CommonDtoContract;

class CategoryRequestDto extends BaseDto implements CommonDtoContract
{
    public \Illuminate\Support\Collection $categories;

    public \Illuminate\Support\Collection $newCategories;
}
