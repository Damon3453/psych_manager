<?php

declare(strict_types=1);

namespace App\Dto;

use App\Contracts\CommonDtoContract;

class TherapyDto extends BaseDto implements CommonDtoContract
{
    public ?int $problem_severity = null;

    public ?string $plan = null;

    public ?string $request = null;

    public ?string $notes = null;

    public ?string $concept_vision;
}
