<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Concerns\QueriesRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Expression;
use Illuminate\Contracts\Cache\Store;
use App\Services\Cache\CacheManager;
use App\Contracts\RepositoryContract;
use Illuminate\Support\Arr;

abstract class BaseRepository implements RepositoryContract
{
    use QueriesRelationships;

    public Builder $query;

    public Model|Collection|self|array|int|Builder|null $result = null;

    /**
     * @var CacheManager
     */
    public Store $cache;

    protected string $model;

    public function __construct()
    {
        $this->cache = app(CacheManager::class);
        $this->initQuery();
    }

    /**
     * Add the "has" condition where clause to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $hasQuery
     * @param \Illuminate\Database\Eloquent\Relations\Relation $relation
     * @param string $operator
     * @param int $count
     * @param string $boolean
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    protected function addHasWhere(Builder $hasQuery, Relation $relation, $operator, $count, $boolean): Builder|static
    {
        $hasQuery->mergeConstraintsFrom($relation->getQuery());

        return $this->canUseExistsForExistenceCheck($operator, $count)
            ? $this->query->addWhereExistsQuery($hasQuery->toBase(), $boolean, $operator === '<' && $count === 1)
            : $this->addWhereCountQuery($hasQuery->toBase(), $operator, $count, $boolean);
    }

    /**
     * @return \App\Models\BaseModel
     */
    final public function getModel(): \App\Models\BaseModel
    {
        return app($this->model);
    }

    /**
     * @param string $model
     *
     * @return $this
     */
    final public function setModel(string $model): static
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return \App\Repositories\BaseRepository
     */
    final public function initQuery(): static
    {
        $this->query = $this->getModel()->queryModel();

        return $this;
        // return $this->model->setConnection($this->connection)->setTable($this->table)->queryModel();
        // return $this->model->setConnection($this->connection)::q();
    }

    /**
     * Find a model by its primary key.
     *
     * @param mixed $id
     * @param array<int, string>|string $columns
     *
     * @return Collection|Model|static|static[]|null
     */
    final public function find(mixed $id, array|string $columns = ['*']): Model|Collection|array|static|null
    {
        return $this->query->find($id, $columns);
    }

    /**
     * Execute the query and get the first result.
     *
     * @param array<int, string>|string $columns
     *
     * @return Model|static|null
     */
    final public function first(array|string $columns = ['*']): Model|null|static
    {
        return $this->query->first($columns);
    }

    /**
     * Get all the models from the database.
     *
     * @param array|string $columns
     *
     * @return Collection<int, static>
     */
    final public function all(array|string $columns = ['*']): Collection
    {
        return $this->getModel()->all($columns);
    }

    /**
     * Add a basic where clause to the query.
     *
     * @param array|\Closure|Expression|string $column
     * @param mixed|null $operator
     * @param mixed|null $value
     * @param string $boolean
     *
     * @return $this|Builder
     */
    final public function where(
        array|\Closure|string|Expression $column,
        mixed $operator = null,
        mixed $value = null,
        string $boolean = 'and'
    ): Builder|static {
        $this->query->where($column, $operator, $value, $boolean);

        return $this;
    }

    /**
     * Find a model by its primary key or throw an exception.
     *
     * @param mixed $id
     * @param array<int, string>|string $columns
     *
     * @throws \Exception<Model>
     *
     * @return Collection|Model|static|static[]
     */
    final public function findOrFail(mixed $id, array|string $columns = ['*']): Model|Collection|array|static
    {
        return $this->query->findOrFail($id, $columns);
    }

    /**
     * Find a model by its primary key or return fresh model instance.
     *
     * @param mixed $id
     * @param array<int, string>|string $columns
     *
     * @return Model|static
     */
    final public function findOrNew(mixed $id, array|string $columns = ['*']): Model|static
    {
        return $this->query->findOrNew($id, $columns);
    }

    /**
     * Get the first record matching the attributes or instantiate it.
     *
     * @param array $attributes
     * @param array $values
     *
     * @return Model|static
     */
    final public function firstOrNew(array $attributes = [], array $values = []): Model|static
    {
        return $this->query->firstOrNew($attributes, $values);
    }

    /**
     * Get the first record matching the attributes or create it.
     *
     * @param array $attributes
     * @param array $values
     *
     * @return Model|static
     */
    final public function firstOrCreate(array $attributes = [], array $values = []): Model|static
    {
        return $this->query->firstOrCreate($attributes, $values);
    }

    /**
     * Create or update a record matching the attributes, and fill it with values.
     *
     * @param array $attributes
     * @param array $values
     *
     * @return Model|static
     */
    final public function updateOrCreate(array $attributes, array $values = []): Model|static
    {
        return $this->query->updateOrCreate($attributes, $values);
    }

    /**
     * Execute the query and get the first result or throw an exception.
     *
     * @param array<int, string>|string $columns
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<Model>
     *
     * @return Model|static
     */
    final public function firstOrFail(array|string $columns = ['*']): Model|static
    {
        return $this->query->firstOrFail($columns);
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param array<int, string>|string $columns
     *
     * @return Collection|static[]
     */
    final public function get(array|string $columns = ['*']): Collection|array
    {
        return $this->query->get($columns);
    }

    /**
     * Save a new model and return the instance.
     *
     * @param array $attributes
     *
     * @return $this|Model
     */
    final public function create(array $attributes = []): Model|static
    {
        return $this->query->create($attributes);
    }

    /**
     * Update the model in the database.
     *
     * @param array $attributes
     * @param array $options
     *
     * @return bool
     */
    final public function update(array $attributes = [], array $options = []): bool
    {
        return $this->getModel()->update($attributes, $options);
    }

    /**
     * Delete records from the database.
     *
     * @return mixed
     */
    final public function delete(): mixed
    {
        return $this->query->delete();
    }

    /**
     * Add a "where in" clause to the query.
     *
     * @param string $column
     * @param mixed $values
     * @param string $boolean
     * @param bool $not
     *
     * @return Builder
     */
    final public function whereIn(string $column, mixed $values, string $boolean = 'and', bool $not = false): Builder
    {
        return $this->query->whereIn(...func_get_args());
    }

    /**
     * Add a where between statement to the query.
     *
     * @param \Illuminate\Database\Query\Expression|string $column
     * @param iterable $values
     * @param string $boolean
     * @param bool $not
     *
     * @return Builder
     */
    final public function whereBetween(
        string|Expression $column,
        iterable $values,
        string $boolean = 'and',
        bool $not = false
    ): Builder {
        return $this->query->whereBetween(...func_get_args());
    }

    /**
     * Retrieve the "count" result of the query.
     *
     * @param string $columns
     *
     * @return int
     */
    final public function count(string $columns = '*'): int
    {
        return (int) $this->query->aggregate(__FUNCTION__, Arr::wrap($columns));
    }

    /**
     * Set the relationships that should be eager loaded.
     *
     * @param array|string $relations
     * @param \Closure|string|null $callback
     *
     * @return Builder|static
     */
    final public function with(array|string $relations, string|\Closure $callback = null): Builder|static
    {
        $this->query->with($relations, $callback);

        return $this;
    }

    /**
     * Add a relationship count / exists condition to the query.
     *
     * @param \Illuminate\Database\Eloquent\Relations\Relation|string $relation
     * @param string $operator
     * @param int $count
     * @param string $boolean
     * @param \Closure|null $callback
     *
     * @throws \RuntimeException
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    final public function has(
        $relation,
        $operator = '>=',
        $count = 1,
        $boolean = 'and',
        \Closure $callback = null
    ): Builder|static {
        if (is_string($relation)) {
            if (str_contains($relation, '.')) {
                return $this->query->hasNested($relation, $operator, $count, $boolean, $callback);
            }

            $relation = $this->getRelationWithoutConstraints($relation);
        }

        if ($relation instanceof MorphTo) {
            return $this->hasMorph($relation, ['*'], $operator, $count, $boolean, $callback);
        }

        // If we only need to check for the existence of the relation, then we can optimize
        // the sub-query to only run a "where exists" clause instead of this full "count"
        // clause. This will make these queries run much faster compared with a count.
        $method = $this->canUseExistsForExistenceCheck($operator, $count)
            ? 'getRelationExistenceQuery'
            : 'getRelationExistenceCountQuery';

        $hasQuery = $relation->{$method}(
            $relation->getRelated()->newQueryWithoutRelationships(),
            $this->query
        );

        // Next we will call any given callback as an "anonymous" scope, so they can get the
        // proper logical grouping of the where clauses if needed by this Eloquent query
        // builder. Then, we will be ready to finalize and return this query instance.
        if ($callback) {
            $hasQuery->callScope($callback);
        }
        $this->addHasWhere(
            $hasQuery, $relation, $operator, $count, $boolean
        );

        return $this;
    }
}
