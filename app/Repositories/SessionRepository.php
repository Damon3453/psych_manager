<?php

declare(strict_types=1);

namespace App\Repositories;

use Carbon\CarbonInterface;
use App\DataObjects\Filters\SessionFilterData;
use App\Models\{User, Session};

class SessionRepository extends BaseRepository
{
    /**
     * @var \App\Builders\FilterBuilder
     */
    public \Illuminate\Database\Eloquent\Builder $query;

    protected string $model = Session::class;

    // public \Illuminate\Database\Eloquent\Builder $filterBuilder;

    // public function __construct(\Illuminate\Database\Query\Builder $builder)
    // {
    //     parent::__construct();
    //     $this->filterBuilder = new FilterBuilder($this->query);
    // }

    /**
     * @param array $sort
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function sort(array $sort = []): \Illuminate\Database\Eloquent\Builder
    {
        // return $this->filterBuilder->sort($sort);
        return $this->query->sort($sort);
    }

    /**
     * @param array $filters
     * @param array|null $total
     * @param \App\Models\User $user
     *
     * @return $this
     */
    public function filtered(array $filters, ?array &$total, User $user): static
    {
        $this->query->where('user_id', $user->id)
            ->has('client')
            ->with([
                'client' => ['category', 'connectionType', 'latestSession'],
            ]);

        $this->query->sessionFilters(SessionFilterData::from($filters['fields'] ?? []));
        $total = $this->query->count();
        $this->query->withPagination($filters['pagination'] ?? [])->sort($filters['sort'] ?? []);

        return $this;
    }

    public function calendar(CarbonInterface $date): static
    {
        $this->query->has('client')
            ->with(['client.connectionType'])
            ->whereBetween(
                'session_date',
                [
                    $date->startOfMonth()->format('Y-m-d H:i'),
                    $date->endOfMonth()->format('Y-m-d H:i'),
                ]
            );
        // ->whereDate('session_date', '>=', $date->format('Y-m-d H:i'))
        // ->whereDate('session_date', '<=', $date->addWeek()->format('Y-m-d H:i'));

        return $this;
    }

    public function getTodaySessions(CarbonInterface $date, User $user): static
    {
        $this->query
            ->select(['id', 'user_id', 'session_date', 'session_time_start', 'session_time_end'])
            ->where('user_id', $user->id)
            ->whereDate('session_date', $date);

        return $this;
    }
}
