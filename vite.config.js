import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'
import vue from '@vitejs/plugin-vue'
import eslint from 'vite-plugin-eslint'

import path from 'path'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import Inspect from 'vite-plugin-inspect'
import vuePugPlugin from 'vue-pug-plugin'

const pathSrc = path.resolve(__dirname, './resources/js')
export default defineConfig({
  server: {
    hmr: {
      host: 'localhost',
      // port: 8000
    },
  },
  plugins: [
    eslint({
      failOnWarning: false,
      failOnError: false,
    }),
    AutoImport({
      imports: ['vue'],
      resolvers: [
        ElementPlusResolver(),
        IconsResolver({
          prefix: 'Icon',
        }),
      ],
      dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
    }),
    Components({
      resolvers: [
        IconsResolver({
          enabledCollections: ['ep'],
        }),
        ElementPlusResolver(),
      ],
      dts: path.resolve(pathSrc, 'components.d.ts'),
    }),
    Icons({
      autoInstall: true,
    }),
    Inspect(),

    laravel({
      input: [
        // 'resources/css/app.css',
        'resources/js/app.js',
        'resources/js/landing/app.js',
      ],
      refresh: true,
    }),
    vue({
      template: {
        preprocessOptions: {
          plugins: [
            vuePugPlugin,
          ],
        },
        transformAssetUrls: {
          base: null,
          includeAbsolute: false,
        },
      },
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './resources/js'),
      '@layout': path.resolve(__dirname, './resources/js/components/layout'),
      '@modules': path.resolve(__dirname, './resources/js/store/modules'),

      '@partials': path.resolve(__dirname, './resources/js/components/_partials'),
      '@mixins': path.resolve(__dirname, './resources/js/components/_mixins'),

      '@auth': path.resolve(__dirname, './resources/js/components/auth'),
      '@clients': path.resolve(__dirname, './resources/js/components/clients'),
      '@sessions': path.resolve(__dirname, './resources/js/components/sessions'),
    },
  },
})
